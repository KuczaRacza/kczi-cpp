#include "filters.hpp"
#include "bitmap.hpp"
#include "utils.hpp"
#include <algorithm>

auto color_region_edges(DeserializedKczi &kczi, Bitmap &decoded) -> void {
  constexpr u32 color = 0xFFE000FF;
  for (auto &r : kczi.regions) {
    for (u32 y = r.vars.pos.y; y < r.vars.pos.y + r.vars.size.y; y++) {
      decoded.set_pixel({0, y}, color);
    }
    for (u32 y = r.vars.pos.y; y < r.vars.pos.y + r.vars.size.y; y++) {
      decoded.set_pixel({(u32)r.vars.size.x + r.vars.pos.x - 1, y}, color);
    }
    for (u32 x = r.vars.pos.x; x < r.vars.pos.x + r.vars.size.x; x++) {
      decoded.set_pixel({x, 0}, color);
    }
    for (u32 x = r.vars.pos.x; x < r.vars.pos.x + r.vars.size.x; x++) {
      decoded.set_pixel(
          {
              x,
              (u32)r.vars.size.y + r.vars.pos.y - 1,
          },
          color);
    }
  }
}
auto color_blocks(DeserializedKczi &kczi, Bitmap &decode) -> void {
  auto max_depth = std::max_element(kczi.regions.begin(), kczi.regions.end(),
                                    [](auto &t1, auto &t2) {
                                      return t1.vars.depth < t2.vars.depth;
                                    })
                       ->vars.depth;
  constexpr u32 colors[4] = {0xFF0000FF, 0xFFFF00FF, 0x00FF00FF, 0xFF00FFFF};
  BitmapRef brf = {decode, {0, 0}, {0, 0}};

  for (auto &r : kczi.regions) {
    u32 block_size =
        (1.0f - r.vars.depth / (float)max_depth) * kczi.vars.block_size;
    block_size = (block_size < 8) ? 8 : block_size;
    block_size /= 2;
    block_size *= 2;
    auto color = [&](u2_32 coord, BitmapRef ref, u2_32 size) {
      u16 bit_pos = coord.y * size.x + coord.x;
      u8 bitshift = (bit_pos % 4 * 2);
      u8 algo = (r.blokcs[bit_pos / 4] & (3 << bitshift)) >> bitshift;
      for (u32 x = 0; x < ref.size.x; x++) {
        ref.set_pixel({x, 0}, colors[algo]);
      }
      for (u32 y = 0; y < ref.size.y; y++) {
        ref.set_pixel({0, y}, colors[algo]);
      }
      for (u32 x = 0; x < ref.size.x; x++) {
        ref.set_pixel({x, ref.size.y - 1}, colors[algo]);
      }
      for (u32 y = 0; y < ref.size.y; y++) {
        ref.set_pixel({ref.size.x - 1, y}, colors[algo]);
      }
    };
    brf.pos = type2_cast<u32>(r.vars.pos);
    brf.size = type2_cast<u32>(r.vars.size);
    r.loop(block_size, color, brf);
  }
}
auto color_by_dict(DeserializedKczi &kczi, Bitmap &decoded) -> void {
  constexpr u32 color[4] = {0xff0000ff, 0xffff0000, 0xff00ff00, 0xff00ffff};
  if (kczi.vars.format == pixel_format::DICTRGB ||
      kczi.vars.format == pixel_format::DICTRGBA) {
    for (auto &r : kczi.regions) {
      BitmapRef b = {decoded, type2_cast<u32>(r.vars.pos),
                     type2_cast<u32>(r.vars.size)};
      b.for_pixel([&](pixel_data &px) {
        pixel_data mask;
        mask.data = color[r.vars.dict_index % 4];
        for (u32 i = 0; i < 3; i++) {
          px.channels[i] = (u16(px.channels[i]) * 2 + u16(mask.channels[i])) / 3;
        }
      });
    }
  }
}