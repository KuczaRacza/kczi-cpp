#include "bitmap.hpp"
#pragma once
#include <kczi_dec.hpp>
#include <kczi_enc.hpp>
#include <serialization.hpp>

auto color_region_edges(DeserializedKczi &kczi, Bitmap &decoded) -> void;
auto color_blocks(DeserializedKczi &kczi, Bitmap &decode) -> void;
auto color_by_dict(DeserializedKczi &kczi, Bitmap &decoded) -> void;
