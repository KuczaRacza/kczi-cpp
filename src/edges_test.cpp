#include <utils.hpp>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_surface.h>
#include <bitmap.hpp>
#include <edges.hpp>
#include <yuv_conversion.hpp>
int main(int argc, char **argv) {
  Bitmap b;
  SDL_Surface *surf = IMG_Load(argv[1]);
  b.load_img(surf);
  BitmapRef ref = {b, {0, 0}, {b.vars.size_x, b.vars.size_y}};
  rgb_to_yuv(ref);
  auto edges_map = create_edges_map(ref);
  edges_map.save_bmp("abc.bmp");
  Bitmap out;
  out.crate(edges_map.vars.size);
  for (u32 y = 0; y < edges_map.vars.size.y; y++) {
    for (u32 x = 0; x < edges_map.vars.size.x; x++) {
		pixel_data px;
		u8 color = edges_map.get_pixel({x, y})[0] / (max_u16 / max_u8);
		px.rgba.r = color;
		px.rgba.g = color;
		px.rgba.b = color;
		px.rgba.a = 0xFF;

      out.set_pixel({x, y},px.data );
    }
  }
  SDL_Surface *sdl_out = out.crate_sdl();
  SDL_SaveBMP(sdl_out, argv[2]);
  SDL_FreeSurface(surf);
  SDL_FreeSurface(sdl_out);
  return 0;
}