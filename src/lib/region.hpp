#pragma once
#include <algorithm>
#include <bitmap.hpp>
#include <utils.hpp>

class UnencRegion;

class Region {

public:
	Region();
	~Region();
	Region(Region &&r);
	Region(UnencRegion &&r);
	Region(Region &r);
	template <typename T>
	auto loop(u32 size, T callbalck, BitmapRef b) const -> void;
	struct {
		u2_16 pos;
		u2_16 size;
		u16 dict_index;
		u8 depth;
	} vars;
	std::vector<u8> pixels;
	std::vector<u8> blokcs;
};
class UnencRegion : public Region {
public:
	UnencRegion(BitmapRef b, u8 depth);
	UnencRegion(UnencRegion &&reg);
	~UnencRegion() = default;
	BitmapRef b;
};

template <typename T>
// callback aruments position(u2_32) area(BitmapRef)  number of blocks(u2_32)
auto Region::loop(u32 size, T callback, BitmapRef b) const -> void {
	u2_32 blk_nr = type2_cast<u32>(vars.size) / ty2<u32>(size, size);
	blk_nr.x = std::max<u32>(1, blk_nr.x);
	blk_nr.y = std::max<u32>(1, blk_nr.y);
	for (u32 y = 0; y < blk_nr.y - 1; y++) {
		for (u32 x = 0; x < blk_nr.x - 1; x++) {
			callback(ty2(x, y), BitmapRef(b.b, b.pos + ty2(x * size, y * size), {size, size}), blk_nr);
		}
		u2_32 re_size = {vars.size.x - (blk_nr.x - 1) * size, size};
		callback(ty2(blk_nr.x - 1, y), BitmapRef(b.b, b.pos + ty2((blk_nr.x - 1) * size, y * size), re_size), blk_nr);
	}
	for (u32 x = 0; x < blk_nr.x - 1; x++) {
		u2_32 le_size = {size, vars.size.y - (blk_nr.y - 1) * size};
		callback(ty2(x, blk_nr.y - 1), BitmapRef(b.b, b.pos + ty2(size * x, (blk_nr.y - 1) * size), le_size), blk_nr);
	}
	u2_32 cr_size = {vars.size.x - (blk_nr.x - 1) * size, vars.size.y - (blk_nr.y - 1) * size};
	u2_32 cr_pos = {(blk_nr.x - 1) * size, (blk_nr.y - 1) * size};
	callback(blk_nr - ty2<u32>(1, 1), BitmapRef(b.b, b.pos + cr_pos, cr_size), blk_nr);
}
