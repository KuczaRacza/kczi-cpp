#include "bitmap.hpp"
#include "utils.hpp"
#include <array>
#include <edges.hpp>
#include <iostream>
auto create_edges_map(BitmapRef b, u32 *mean) -> MultiChannelBitmap<1, u16> {
	MultiChannelBitmap<1, u16> map(b.size);
	u32 edges_mean = 0;
	for (u32 y = 1; y < b.size.y - 1; y++) {
		for (u32 x = 1; x < b.size.x - 1; x++) {
			i32 gx = 0;
			i32 gy = 0;
			pixel_data px;
			px.data = b.get_pixel(ty2(x - 1, y - 1));
			gx += px.yuva.y * -1;
			px.data = b.get_pixel(ty2(x - 1, y));
			gx += px.yuva.y * -2;
			px.data = b.get_pixel(ty2(x - 1, y + 1));
			gx += px.yuva.y * -1;
			px.data = b.get_pixel(ty2(x + 1, y - 1));
			gx += px.yuva.y;
			px.data = b.get_pixel(ty2(x + 1, y));
			gx += px.yuva.y * 2;
			px.data = b.get_pixel(ty2(x + 1, y + 1));
			gx += px.yuva.y;

			px.data = b.get_pixel(ty2(x + 1, y - 1));
			gy += px.yuva.y;
			px.data = b.get_pixel(ty2(x, y - 1));
			gy += px.yuva.y * 2;
			px.data = b.get_pixel(ty2(x - 1, y - 1));
			gy += px.yuva.y;
			px.data = b.get_pixel(ty2(x - 1, y + 1));
			gy += px.yuva.y * -1;
			px.data = b.get_pixel(ty2(x, y + 1));
			gy += px.yuva.y * -2;
			px.data = b.get_pixel(ty2(x + 1, y + 1));
			gy += px.yuva.y * -1;
			u16 edge = gx * gx + gy * gy;
			map.set_pixel({x, y}, {edge});
			edges_mean += edge;
		}
	}
	edges_mean = edges_mean * 8 / b.size.get_product();
	if (mean) {
		*mean = edges_mean;
	}
	return map;
}
auto is_drawing(MultiChannelBitmap<1, u16> &edges, u32 mean_edges, u8 block_size) -> bool {
	for (u32 y = 0; y < edges.vars.size.y / block_size; y++) {
		for (u32 x = 0; x < edges.vars.size.x / block_size; x++) {
			MultiBitmapRef<1, u16> ref{edges, {x * block_size, y * block_size}, {block_size, block_size}};
			u32 block_edges = 0;
			ref.for_pixel([&](std::array<u16, 1> &px) {
				block_edges += px[0];
			});
			block_edges = block_edges * 32 / ref.size.get_product();
		}
	}
	return true;
}