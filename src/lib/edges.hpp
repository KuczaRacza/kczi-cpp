#include <bitmap.hpp>
//mean is multipled  by 8 
auto create_edges_map(BitmapRef b, u32 *mean = nullptr) -> MultiChannelBitmap<1, u16>;
auto is_drawing(MultiChannelBitmap<1, u16> &edges, u32  mean_edges,u8 block_size) -> bool;
