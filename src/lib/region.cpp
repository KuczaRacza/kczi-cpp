#include "region.hpp"
Region::Region() {}
Region::Region(Region &r) : vars(r.vars), blokcs(r.blokcs), pixels(r.pixels) {}
Region::Region(Region &&r) : vars(r.vars), blokcs(std::move(r.blokcs)), pixels(std::move(r.pixels)) {}
Region::Region(UnencRegion &&r) : vars(r.vars) {
	blokcs.swap(r.blokcs);
	pixels.swap(r.pixels);
}
Region::~Region() {}

UnencRegion::UnencRegion(BitmapRef b, u8 depth) : Region(), b(b) {
	Region::vars.depth = depth;
	Region::vars.size = type2_cast<u16>(b.size);
	Region::vars.pos = type2_cast<u16>(b.pos);
}
UnencRegion::UnencRegion(UnencRegion &&reg) : Region(std::move(reg)), b(reg.b) {}