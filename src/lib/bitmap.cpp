#include "bitmap.hpp"
#include "utils.hpp"
#include <SDL2/SDL_surface.h>
#include <cstdlib>
Bitmap::Bitmap() {
	vars.size_x = 0;
	vars.size_y = 0;
}
Bitmap::~Bitmap() {}

auto Bitmap::load_img(SDL_Surface *surf) -> void {
	SDL_Surface *tmp = SDL_ConvertSurfaceFormat(surf, SDL_PIXELFORMAT_RGBA32, 0);
	data = std::move(std::unique_ptr<pixel_data, px_deleter>(new pixel_data[tmp->w * tmp->h]));
	u8 *src_img = (u8 *)tmp->pixels;
	u8 *dst_img = (u8 *)data.get();
	for (u32 i = 0; i < (u32)tmp->h; i++) {
		memcpy(dst_img + i * tmp->w * sizeof(pixel_data), src_img + i * tmp->pitch, tmp->w * sizeof(u32));
	}
	vars.size_x = tmp->w;
	vars.size_y = tmp->h;
	SDL_FreeSurface(tmp);
}
auto Bitmap::crate_sdl() -> SDL_Surface * {
	u8 *surf_data = (u8 *)malloc(vars.size_x * vars.size_y * sizeof(pixel_data));
	memcpy(surf_data, data.get(), vars.size_x * vars.size_y * sizeof(pixel_data));
	SDL_Surface *surf = SDL_CreateRGBSurfaceWithFormatFrom(surf_data, vars.size_x, vars.size_y, 32, vars.size_x * sizeof(pixel_data), SDL_PIXELFORMAT_RGBA32);
	return surf;
}

Bitmap::Bitmap(Bitmap &&b) : vars(b.vars), data(std::move(b.data)) {}
BitmapRef::BitmapRef(Bitmap &b, u2_32 pos, u2_32 size)
		: b(b), pos(pos), size(size) {}
BitmapRef::~BitmapRef() {}

auto Bitmap::crate(u2_32 size) -> void {
	data.reset(new pixel_data[size.get_product()]);
	vars.size_x = size.x;
	vars.size_y = size.y;
}
auto Bitmap::save_bmp(std::string_view path) -> void{
	auto sdl = crate_sdl();
	SDL_SaveBMP(sdl, path.data());
	SDL_FreeSurface(sdl);
}