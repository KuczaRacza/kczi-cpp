#pragma once
#include <span>
#include <unordered_map>
#include <utils.hpp>
#include <vector>

class Quant_matrix_dct : public std::vector<u16> {
public:
	Quant_matrix_dct(u64 size);
	Quant_matrix_dct(Quant_matrix_dct && ) =default;

};

class Cache {
public:
	Cache() = default;
	~Cache() = default;
	u2_32 img_dct_quant;
	std::unordered_map<u2_32, Quant_matrix_dct> dct_q_cache;
	auto get_q_matrix(u2_32 size) -> const Quant_matrix_dct &;
};