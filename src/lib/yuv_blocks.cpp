#include "cache.hpp"
#include "utils.hpp"
#include <algorithm>
#include <alloca.h>
#include <array>
#include <cmath>
#include <cstring>
#include <optional>
#include <span>
#include <yuv_blocks.hpp>
template <bool alpha>
static inline auto dct_quantize(std::span<i16> coefficients, std::span<const u16> quant_matrix) {
	constexpr u8 bpp = 3 + alpha;
	for (u32 i = 0; i < quant_matrix.size(); i++) {
		for (u8 j = 0; j < bpp; j++) {
			coefficients[i * bpp + j] = coefficients[i * bpp + j] / quant_matrix[i] + (coefficients[i * bpp + j] % quant_matrix[i] > quant_matrix[i] / 2);
		}
	}
}
template <bool alpha>

static inline auto dct_dequantize(std::span<i16> coefficients, std::span<const u16> quant_matrix) {
	constexpr u8 bpp = 3 + alpha;
	for (u32 i = 0; i < quant_matrix.size(); i++) {
		for (u8 j = 0; j < bpp; j++) {
			coefficients[i * bpp + j] = std::clamp<i32>(coefficients[i * bpp + j] * quant_matrix[i], max_i16 * -1, max_i16);
		}
	}
}
auto dct_create_matrix(std::span<u16> data, u32 start, u32 end, u2_32 size) -> void {
	for (u32 y = 0; y < size.y; y++) {
		for (u32 x = 0; x < size.x; x++) {
			float distance = (float)(x * x + y * y) / (float)(size.x * size.x + size.y * size.y);
			data[y * size.x + x] = distance * (end - start) + start;
		}
	}
}
template <u8 bpp>
static inline auto ind_buff(u2_32 pos, u2_32 size, u8 channel) {
	return (u32)(pos.y * size.x + pos.x) * bpp + channel;
}
static inline auto normalize_c(const u32 &color, u8 channel) {
	pixel_data px;
	px.data = color;
	auto &val = px.channels[channel];
	return val / 127.5f - 1.0f;
}
static inline auto cos_dct(const u32 &x, const u32 &m, const u32 &size) {
	return cosf((M_PI / (float)size) * (x + 0.5f) * m);
}
constexpr static inline auto cos_dct_const(const u32 x, const u32 m, const u32 size) {
	return cosf((M_PI / (float)size) * (x + 0.5f) * m);
}
template <u2_16 size, u8 bpp>
constexpr auto get_cosf_const(const u32 x, const u32 y, const u32 m, const u32 n, const u32 c) -> u16 {
	return bpp * (size.x * (size.y * (y * size.x + x) + n) + m) + c;
}
template <u2_16 size, u8 bpp>
[[gnu::hot]] constexpr auto generte_cos_dct_table() {
	std::array<float, size.get_product() * size.get_product() * bpp> cos_table;
	for (u32 y = 0; y < size.y; y++) {
		for (u32 x = 0; x < size.x; x++) {
			for (u32 n = 0; n < size.y; n++) {
				for (u32 m = 0; m < size.x; m++) {
					for (u32 c = 0; c < bpp; c++) {
						cos_table[get_cosf_const<size, bpp>(x, y, m, n, c)] = cosf((M_PI / (float)size.x) * (x + 0.5f) * m) * cosf((M_PI / (float)size.y) * (y + 0.5f) * n) / size.get_product();
					}
				}
			}
		}
	}
	return cos_table;
}
template <bool alpha, u2_16 size = ty2<u16>(0, 0)>
static inline auto yuv_dct_block(std::span<i16> data, const BitmapRef &block) {
	constexpr u8 bpp = 3 + alpha;
	std::array<float, size.get_product() * size.get_product() * bpp> static_cos;
	if constexpr (size.x != 0) {
		static_cos = generte_cos_dct_table<size, bpp>();
	}
	auto &bsize = block.size;
	float *dct_buffer = (float *)alloca(sizeof(float) * block.size.get_product() * bpp);
	memset(dct_buffer, 0, block.size.get_product() * bpp * sizeof(float));

	for (u32 y = 0; y < block.size.y; y++) {
		for (u32 x = 0; x < block.size.x; x++) {
			for (u32 n = 0; n < block.size.y; n++) {
				for (u32 m = 0; m < block.size.x; m++) {
					u32 pixel = block.get_pixel({x, y});
					for (u32 c = 0; c < bpp; c++) {
						float coefficient_f;
						if constexpr (size.x == 0) {
							coefficient_f = normalize_c(pixel, c) * cos_dct(x, m, bsize.x) * cos_dct(y, n, bsize.y) / bsize.get_product();
						} else {
							coefficient_f = normalize_c(pixel, c) * static_cos[get_cosf_const<size, bpp>(x, y, m, n, c)];
						}
						dct_buffer[ind_buff<bpp>({m, n}, bsize, c)] += coefficient_f;
					}
				}
			}
		}
	}
	u32 offset = 0;
	for (u32 y = 0; y < block.size.y; y++) {
		for (u32 x = 0; x < block.size.x; x++) {
			for (u32 c = 0; c < bpp; c++) {
				auto coefficient_f = dct_buffer[ind_buff<bpp>({x, y}, bsize, c)];
				i16 coefficient = std::clamp(coefficient_f, -1.0f, 1.0f) * max_i16;
				data[offset++] = coefficient;
			}
		}
	}
}

static inline auto normalize_i16(const i16 &val) {
	return (float)val / max_i16 * 4.0f;
}
static inline auto cos_idct(const u32 &x, const u32 &m, const u32 &size) {
	return cosf((M_PI / (float)size) * (x + 0.5f) * m);
}
template <u8 bpp>
static inline auto get_cf(const std::span<const i16> &data, u2_32 pos, u2_32 size, u8 channel) {
	auto val = data[(size.x * pos.y + pos.x) * bpp + channel];
	return (float)val * 4.0f / max_i16;
}
template <bool alpha>
static inline auto yuv_idct_block(std::span<const i16> data, BitmapRef block) {
	auto &bsize = block.size;
	constexpr u8 bpp = 3 + alpha;
	float *idct_buffer = (float *)alloca(sizeof(float) * bsize.get_product() * bpp);
	for (u32 y = 0; y < bsize.y; y++) {
		for (u32 x = 0; x < bsize.x; x++) {
			for (u32 c = 0; c < bpp; c++) {
				idct_buffer[ind_buff<bpp>({x, y}, bsize, c)] = get_cf<bpp>(data, {0, 0}, bsize, c) * 0.25f;
			}
		}
	}
	for (u32 y = 0; y < bsize.y; y++) {
		for (u32 x = 0; x < bsize.x; x++) {
			for (u32 n = 1; n < bsize.y; n++) {
				for (u32 m = 1; m < bsize.x; m++) {
					for (u32 c = 0; c < bpp; c++) {
						idct_buffer[ind_buff<bpp>({x, y}, bsize, c)] += get_cf<bpp>(data, {m, n}, bsize, c) * cos_idct(x, m, bsize.x) * cos_idct(y, n, bsize.y);
					}
				}
			}
		}
	}

	for (u32 y = 0; y < bsize.y; y++) {
		for (u32 x = 0; x < bsize.x; x++) {
			for (u32 m = 1; m < bsize.x; m++) {
				for (u32 c = 0; c < bpp; c++) {
					idct_buffer[ind_buff<bpp>({x, y}, bsize, c)] += get_cf<bpp>(data, {m, 0}, bsize, c) * cos_idct(x, m, bsize.x) * 0.5f;
				}
			}
		}
	}
	for (u32 y = 0; y < bsize.y; y++) {
		for (u32 x = 0; x < bsize.x; x++) {
			for (u32 n = 1; n < bsize.y; n++) {
				for (u32 c = 0; c < bpp; c++) {
					idct_buffer[ind_buff<bpp>({x, y}, bsize, c)] += get_cf<bpp>(data, {0, n}, bsize, c) * cos_idct(y, n, bsize.y) * 0.5f;
				}
			}
		}
	}
	for (u32 y = 0; y < bsize.y; y++) {
		for (u32 x = 0; x < bsize.x; x++) {
			pixel_data px{};
			for (u32 c = 0; c < bpp; c++) {
				auto pixel_val = (idct_buffer[ind_buff<bpp>({x, y}, bsize, c)] + 1.0f) * 127.5f;
				px.channels[c] = std::clamp<i32>(pixel_val, 0, 255);
			}
			if constexpr (!alpha) {
				px.yuva.a = 0xff;
			}
			block.set_pixel({x, y}, px.data);
		}
	}
}

template <u8 bpp>
static inline auto copy_to_buffer_zig(std::span<i16> src, std::span<i16> dst, u2_32 pos, u2_32 size, u32 &offset) {
	for (u32 i = 0; i < bpp; ++i) {
		dst[offset++] = src[(pos.x + pos.y * size.x) * bpp + i];
	}
}
template <u8 bpp>
static inline auto copy_to_buffer_xy(std::span<i16> src, std::span<i16> dst, u2_32 pos, u2_32 size, u32 &offset) {
	for (u32 i = 0; i < bpp; i++) {
		dst[(pos.x + pos.y * size.x) * bpp + i] = src[offset++];
	}
}
template <u8 bpp, bool to_zig_zag = true>
auto loop_translate(std::span<i16> data, u2_32 size) {
	u32 offset = 0;
	enum class loop_dir : u8 {
		LEFT_DOWN,
		RIGHT_UP
	};
	i16 *buffer = (i16 *)alloca(data.size() * sizeof(i16));
	auto out = std::span(buffer, data.size());
	auto dir = loop_dir::RIGHT_UP;
	u2_32 pos = {0, 0};
	while (offset < data.size()) {
		if constexpr (to_zig_zag) {
			copy_to_buffer_zig<bpp>(data, out, pos, size, offset);
		} else {
			copy_to_buffer_xy<bpp>(data, out, pos, size, offset);
		}
		if (dir == loop_dir::LEFT_DOWN) {
			if (pos.y == size.y - 1) {
				pos.x++;
				dir = loop_dir::RIGHT_UP;
			} else if (pos.x == 0) {
				dir = loop_dir::RIGHT_UP;
				if (pos.y == size.y - 1) {
					pos.x++;
				} else {
					pos.y++;
				}
			} else [[likely]] {
				pos.x--;
				pos.y++;
			}
		} else {
			if (pos.x == size.x - 1) {
				pos.y++;
				dir = loop_dir::LEFT_DOWN;
			} else if (pos.y == 0) {
				dir = loop_dir::LEFT_DOWN;
				if (pos.x == size.x - 1) {
					pos.y++;
				} else {
					pos.x++;
				}
			} else [[likely]] {
				pos.x++;
				pos.y--;
			}
		}
	}
	std::copy(out.begin(), out.end(), data.begin());
}
template <bool alpha>
static inline auto enc_dct(std::vector<u8> &data, const BitmapRef &block, Cache &cache) {
	constexpr u8 bpp = 3 + alpha;
	const u32 blk_size = block.size.get_product() * bpp * sizeof(i16);
	data.resize(data.size() + blk_size);
	auto data_span = std::span((i16 *)(data.data() + data.size() - blk_size), blk_size / 2);
	if (block.size == ty2<u32>(8, 8)) [[likely]] {
		yuv_dct_block<alpha, ty2<u16>(8, 8)>(data_span, block);
	} else if (block.size == ty2<u32>(10, 10)) {
		yuv_dct_block<alpha, ty2<u16>(10, 10)>(data_span, block);
	} else {
		yuv_dct_block<alpha>(data_span, block);
	}

	auto &qtablevec = cache.get_q_matrix(block.size);
	auto qtable = std::span(qtablevec.data(), qtablevec.size());
	dct_quantize<alpha>(data_span, qtable);
	loop_translate<bpp, true>(data_span, block.size);
}
template <bool alpha>
static inline auto dec_dct(std::span<u8> &data, BitmapRef ref, Cache &cache) {
	constexpr u8 bpp = 3 + alpha;
	std::span<i16> data_span((i16 *)data.data(), bpp * ref.size.get_product());
	loop_translate<bpp, false>(data_span, ref.size);
	auto &qmatrix = cache.get_q_matrix(ref.size);
	std::span<const u16> qmatrix_span(qmatrix.data(), qmatrix.size());
	dct_dequantize<alpha>(data_span, qmatrix_span);
	yuv_idct_block<alpha>(data_span, ref);
	data = std::span<u8>(data.data() + ref.size.get_product() * bpp * 2, data.size() - ref.size.get_product() * bpp * 2);
}
auto yuv_dct_alpha_enc(std::vector<u8> &data, const BitmapRef &block, Cache &cache) -> void {
	enc_dct<true>(data, block, cache);
}
auto yuv_dct_enc(std::vector<u8> &data, const BitmapRef &block, Cache &cache) -> void {
	enc_dct<false>(data, block, cache);
}
auto yuv_dct_alpha_dec(std::span<u8> &data, const BitmapRef &block, Cache &cache) -> void {
	dec_dct<true>(data, block, cache);
}
auto yuv_dct_dec(std::span<u8> &data, const BitmapRef &block, Cache &cache) -> void {
	dec_dct<false>(data, block, cache);
}