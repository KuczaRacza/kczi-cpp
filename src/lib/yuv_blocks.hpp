#pragma once
#include <array>
#include <bitmap.hpp>
#include <span>
#include <string_view>
#include <unordered_map>
#include <utils.hpp>
#include <vector>
#include <cache.hpp>
template <bool alpha>
inline auto yuv_subsample_enc(std::vector<u8> &data, BitmapRef block) -> void {
	for (u32 y = 0; y < block.size.y / 2; y++) {
		for (u32 x = 0; x < block.size.x / 2; x++) {
			u32 u_sample = 0;
			u32 v_sample = 0;
			pixel_data px;
			for (u32 i = 0; i < 2; i++) {
				for (u32 j = 0; j < 2; j++) {
					px.data = block.get_pixel(ty2<u32>(x * 2 + j, y * 2 + i));
					data.push_back(px.yuva.y);
					u_sample += px.yuva.u;
					v_sample += px.yuva.v;
					if constexpr (alpha) {
						data.push_back(px.yuva.a);
					}
				}
			}
			u_sample = u_sample / 4 + (u_sample & 2);
			v_sample = v_sample / 4 + (v_sample & 2);
			data.push_back(u_sample);
			data.push_back(v_sample);
		}
	}
	if (block.size.x & 1) [[unlikely]] {
		for (u32 y = 0; y < block.size.y - 1; y++) {
			pixel_data px;
			px.data = block.get_pixel(ty2(block.size.x - 1, y));
			data.push_back(px.yuva.y);
			data.push_back(px.yuva.u);
			data.push_back(px.yuva.v);
			if constexpr (alpha) {
				data.push_back(px.yuva.a);
			}
		}
	}
	if (block.size.y & 1) [[unlikely]] {
		for (u32 x = 0; x < block.size.x - 1; x++) {
			pixel_data px;
			px.data = block.get_pixel(ty2<u32>(x, block.size.y - 1));
			data.push_back(px.yuva.y);
			data.push_back(px.yuva.u);
			data.push_back(px.yuva.v);
			if constexpr (alpha) {
				data.push_back(px.yuva.a);
			}
		}
	}
	if ((block.size.x & 1) || (block.size.y & 1)) [[unlikely]] {
		pixel_data px;
		px.data = block.get_pixel(block.size - ty2<u32>(1, 1));
		data.push_back(px.yuva.y);
		data.push_back(px.yuva.u);
		data.push_back(px.yuva.v);
		if constexpr (alpha) {
			data.push_back(px.yuva.a);
		}
	}
}
template <bool alpha>
inline auto yuv_subsample_dec(std::span<u8> &data, BitmapRef block) -> void {
	u32 offset = 0;
	for (u32 y = 0; y < block.size.y / 2; y++) {
		for (u32 x = 0; x < block.size.x / 2; x++) {

			pixel_data px;
			if constexpr (alpha) {
				px.yuva.u = data[offset + 8];
				px.yuva.v = data[offset + 9];

			} else {
				px.yuva.u = data[offset + 4];
				px.yuva.v = data[offset + 5];
			}
			for (u32 i = 0; i < 2; i++) {
				for (u32 j = 0; j < 2; j++) {
					px.yuva.y = data[offset++];
					if constexpr (alpha) {
						px.yuva.a = data[offset++];
					} else {
						px.yuva.a = 0xff;
					}
					block.set_pixel(ty2<u32>(x * 2 + j, y * 2 + i), px.data);
				}
			}
			// u v  samlpe
			offset += 2;
		}
	}
	if (block.size.x & 1) [[unlikely]] {
		for (u32 y = 0; y < block.size.y - 1; y++) {
			pixel_data px;
			px.yuva.y = data[offset++];
			px.yuva.u = data[offset++];
			px.yuva.v = data[offset++];
			if constexpr (alpha) {
				px.yuva.a = data[offset++];
			} else {
				px.yuva.a = 0xff;
			}
			block.set_pixel(ty2<u32>(block.size.x - 1, y), px.data);
		}
	}
	if (block.size.y & 1) [[unlikely]] {
		for (u32 x = 0; x < block.size.x - 1; x++) {
			pixel_data px;
			px.yuva.y = data[offset++];
			px.yuva.u = data[offset++];
			px.yuva.v = data[offset++];
			if constexpr (alpha) {
				px.yuva.a = data[offset++];
			} else {
				px.yuva.a = 0xff;
			}
			block.set_pixel(ty2<u32>(x, block.size.y - 1), px.data);
		}
	}
	if ((block.size.x & 1) || (block.size.y & 1)) {
		pixel_data px;
		px.data = block.get_pixel(block.size - ty2<u32>(1, 1));
		px.yuva.y = data[offset++];
		px.yuva.u = data[offset++];
		px.yuva.v = data[offset++];
		if constexpr (alpha) {
			px.yuva.a = data[offset++];
		} else {
			px.yuva.a = 0xff;
		}
		block.set_pixel(ty2<u32>(block.size.x - 1, block.size.y - 1), px.data);
	}
	data = std::span<u8>((u8*)data.data() + offset, data.size() - offset);
}
template <bool alpha>
inline auto yuv_grad_enc(std::vector<u8> &data, BitmapRef ref) {
	pixel_data px;
	px.data = ref.get_pixel(ty2<u32>(0, 0));
	data.push_back(px.yuva.y);
	data.push_back(px.yuva.u);
	data.push_back(px.yuva.v);
	if constexpr (alpha) {
		data.push_back(px.yuva.a);
	}
	px.data = ref.get_pixel(ty2<u32>(ref.size.x - 1, 0));
	data.push_back(px.yuva.y);
	data.push_back(px.yuva.u);
	data.push_back(px.yuva.v);
	if constexpr (alpha) {
		data.push_back(px.yuva.a);
	}
	px.data = ref.get_pixel(ty2<u32>(ref.size.x - 1, ref.size.y - 1));
	data.push_back(px.yuva.y);
	data.push_back(px.yuva.u);
	data.push_back(px.yuva.v);
	if constexpr (alpha) {
		data.push_back(px.yuva.a);
	}
	px.data = ref.get_pixel(ty2<u32>(0, ref.size.y - 1));
	data.push_back(px.yuva.y);
	data.push_back(px.yuva.u);
	data.push_back(px.yuva.v);
	if constexpr (alpha) {
		data.push_back(px.yuva.a);
	}
}
namespace interpolation_yuv {

template <u2_32 size, u2_32 coord, u8 rot>
inline constexpr auto static_interpolation_mul() {
	if constexpr (rot == 0) {
		u32 horizontal = (size.x - coord.x) * max_u16 / size.x;
		u32 vertical = horizontal * (size.y - coord.y) / size.y;
		return vertical;
	} else if constexpr (rot == 1) {
		u32 horizontal = coord.x * max_u16 / size.x;
		u32 vertical = horizontal * (size.y - coord.y) / size.y;
		return vertical;
	} else if constexpr (rot == 2) {
		u32 horizontal = coord.x * max_u16 / size.x;
		u32 vertical = horizontal * coord.y / size.y;
		return vertical;
	} else if constexpr (rot == 3) {
		u32 horizontal = (size.x - coord.x) * max_u16 / size.x;
		u32 vertical = horizontal * coord.y / size.y;
		return vertical;
	}
}
template <bool alpha>
static inline auto dynamic_interpolation(BitmapRef ret, const std::span<u8> &data) {
	constexpr u8 bpp = 3 + alpha;
	for (u32 y = 0; y < ret.size.y; y++) {
		for (u32 x = 0; x < ret.size.x; x++) {
			pixel_data px;
			std::array<u32, 4> interpolation_corners;
			std::array<u32, 4> colors;
			colors.fill(0);
			interpolation_corners[0] = ((ret.size.x - x) * max_u16 / ret.size.x) * (ret.size.y - y) / ret.size.y;
			interpolation_corners[1] = (x * max_u16 / ret.size.x) * (ret.size.y - y) / ret.size.y;
			interpolation_corners[2] = (x * max_u16 / ret.size.x) * y / ret.size.y;
			interpolation_corners[3] = ((ret.size.x - x) * max_u16 / ret.size.x) * y / ret.size.y;
			for (u8 k = 0; k < 4; k++) {
				for (u32 c = 0; c < bpp; c++) {
					colors[c] += interpolation_corners[k] * data[k * bpp + c];
				}
			}

			for (u32 c = 0; c < bpp; c++) {
				u16 roundup = colors[c] & ((max_u16) / 2);
				px.channels[c] = colors[c] / (max_u16);
				if (roundup) {
					if (px.channels[c] != 255)
						px.channels[c]++;
				}
			}
			if constexpr (!alpha) {
				px.yuva.a = 0xff;
			}
			ret.set_pixel({x, y}, px.data);
		}
	}
}
}; // namespace interpolation_yuv
template <bool alpha>
auto yuv_grad_dec(BitmapRef b, std::span<u8> &data) -> void {
	constexpr u32 bpp = 3 + alpha;
	if (std::equal(data.begin(), data.begin() + 4 * bpp, data.rbegin())) {
		pixel_data px;
		px.yuva.y = data[0];
		px.yuva.u = data[1];
		px.yuva.v = data[2];
		if constexpr (alpha) {
			px.yuva.a = data[3];
		} else {
			px.yuva.a = 0xff;
		}
		b.for_pixel([&](pixel_data &ref_px) {
			ref_px = px;
		});
	} else {
		if (b.size.x == b.size.y) {
			interpolation_yuv::dynamic_interpolation<alpha>(b, data);
		} else {
			interpolation_yuv::dynamic_interpolation<alpha>(b, data);
		}
	}
	data = std::span<u8>((u8*)data.data() + bpp * 4, data.size() - bpp * 4);
}
auto yuv_dct_alpha_enc(std::vector<u8> &data, const BitmapRef &block,Cache & cache) -> void;
auto yuv_dct_enc(std::vector<u8> &data, const BitmapRef &block,Cache & cache) -> void;
auto yuv_dct_alpha_dec(std::span<u8> &data, const BitmapRef &block,Cache & cache) -> void;
auto yuv_dct_dec(std::span<u8> &data, const BitmapRef &block,Cache & cache) -> void;
auto dct_create_matrix(std::span<u16> data, u32 start, u32 end, u2_32 size) -> void;