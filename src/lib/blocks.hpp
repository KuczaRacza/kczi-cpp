#pragma once
#include <array>
#include <bitmap.hpp>
#include <constexpr_util.hpp>
#include <serialization.hpp>
#include <span>
#include <string_view>
#include <type_traits>
#include <utils.hpp>
#include <vector>

auto indexed_interpolation_dynamic(const u8 *data, const Dict &d, BitmapRef ret) -> u32;
auto indexed_full_blocks(const u8 *data, const Dict &d, BitmapRef ret) -> u32;
auto indexed_interpolation(const std::basic_string_view<u8> data, const Dict &d,BitmapRef ret) -> u32;

inline auto enc_indx_full(BitmapRef bmap, std::vector<u32> &out) -> void;
inline auto enc_indx_gradient(BitmapRef bmap, std::vector<u32> &out) -> void;

inline auto enc_indx_full(BitmapRef bmap, std::vector<u32> &out) -> void {
	bmap.for_pixel([&](pixel_data &px) { out.push_back(px.data); });
}
inline auto enc_indx_gradient(BitmapRef bmap, std::vector<u32> &out) -> void {
	out.push_back(bmap.get_pixel({0, 0}));
	out.push_back(bmap.get_pixel({bmap.size.x - 1, 0}));
	out.push_back(bmap.get_pixel({bmap.size.x - 1, bmap.size.y - 1}));
	out.push_back(bmap.get_pixel({0, bmap.size.y - 1}));
}
