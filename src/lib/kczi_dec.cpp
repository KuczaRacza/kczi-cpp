#include "kczi_dec.hpp"
#include "bitmap.hpp"
#include "kczi_enc.hpp"
#include "region.hpp"
#include "serialization.hpp"
#include <algorithm>
#include <array>
#include <blocks.hpp>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <functional>
#include <iostream>
#include <iterator>
#include <span>
#include <string_view>
#include <utils.hpp>
#include <vector>
#include <yuv_blocks.hpp>
#include <yuv_conversion.hpp>
KcziDec::KcziDec() {}
KcziDec::~KcziDec() {}
static inline auto blok_size_calc(const Region &reg, const DeserializedKczi &kczi) {
	u32 block_size = (1.0f - reg.vars.depth / (float)kczi.vars.max_depth) * kczi.vars.block_size;
	block_size = (block_size < 8) ? 8 : block_size;
	block_size /= 2;
	block_size *= 2;
	return block_size;
}
static inline auto get_algo(const u2_32 &size, const u2_32 &coord, const std::vector<u8> &algo_vec) {
	u16 bit_pos = coord.y * size.x + coord.x;
	u8 bitshift = (bit_pos % 4 * 2);
	u8 algo = (algo_vec[bit_pos / 4] & (3 << bitshift)) >> bitshift;
	return algo;
}
static auto decode_region_indexes(BitmapRef ref, Region &reg, const KcziDec &dec) -> void {
	u32 offset = 0;
	u32 block_size = blok_size_calc(reg, dec.kczi_in);
	const Dict &d = dec.kczi_in.dicts[reg.vars.dict_index];
	auto per_block = [&](u2_32 coord, BitmapRef ref, u2_32 bl_size) {
		BLOCK_ALGO_INX algo = (BLOCK_ALGO_INX)get_algo(bl_size, coord, reg.blokcs);
		if (algo == BLOCK_ALGO_INX::FULL_RES) {
			offset += indexed_full_blocks(reg.pixels.data() + offset, d, ref);
		} else {
			offset += indexed_interpolation(std::basic_string_view<u8>(reg.pixels.data() + offset, 4), d, ref);
		}
	};
	reg.loop(block_size, per_block, ref);
}
template <bool alpha>
static auto decode_region_real(BitmapRef ref, const Region &reg, KcziDec &dec) -> void {
	auto data_stream = std::span((u8 *)reg.pixels.data(), reg.pixels.size());
	const u32 block_size = blok_size_calc(reg, dec.kczi_in);

	auto per_block = [&](u2_32 coord, BitmapRef block_ref, u2_32 bl_size) {
		BLOCK_ALGO_YUV algo = (BLOCK_ALGO_YUV)get_algo(bl_size, coord, reg.blokcs);
		if (algo == BLOCK_ALGO_YUV::SUBSAMPLED) {
			yuv_subsample_dec<alpha>(data_stream, block_ref);
		} else if (algo == BLOCK_ALGO_YUV::GRAD) {
			yuv_grad_dec<alpha>(block_ref, data_stream);
		} else if (algo == BLOCK_ALGO_YUV::DCT) {
			if constexpr (alpha) {
				yuv_dct_alpha_dec(data_stream, block_ref, dec.cache);
			} else {
				yuv_dct_dec(data_stream, block_ref, dec.cache);
			}
		}
	};
	reg.loop(block_size, per_block, ref);
	yuv_to_rgb(ref);
}
auto KcziDec::decode_to_bitmap(std::vector<u8> &data) -> Bitmap {
	u16 f_ok = kczi_in.deserialize(data);
	if (f_ok != file_errors::OK) {
		return Bitmap();
	}
	out_img.crate({kczi_in.vars.x, kczi_in.vars.y});
	max_depth = kczi_in.regions.front().vars.depth;
	cache.img_dct_quant = {kczi_in.vars.dct_start, kczi_in.vars.dct_end};
	for (u32 i = 0; i < kczi_in.vars.length; i++) {
		if (max_depth < kczi_in.regions[i].vars.depth) {
			max_depth = kczi_in.regions[i].vars.depth;
		}
	}
	if (kczi_in.vars.format == pixel_format::YUV) {
		for (u32 i = 0; i < kczi_in.vars.length; i++) {
			u2_32 pos_32 = {kczi_in.regions[i].vars.pos.x, kczi_in.regions[i].vars.pos.y};
			u2_32 size_32 = {kczi_in.regions[i].vars.size.x, kczi_in.regions[i].vars.size.y};
			decode_region_real<false>({out_img, pos_32, size_32}, kczi_in.regions[i], *this);
		}
	} else if (kczi_in.vars.format == pixel_format::YUVA) {
		for (u32 i = 0; i < kczi_in.vars.length; i++) {
			u2_32 pos_32 = {kczi_in.regions[i].vars.pos.x, kczi_in.regions[i].vars.pos.y};
			u2_32 size_32 = {kczi_in.regions[i].vars.size.x, kczi_in.regions[i].vars.size.y};
			decode_region_real<true>({out_img, pos_32, size_32}, kczi_in.regions[i], *this);
		}
	} else {
		for (u32 i = 0; i < kczi_in.vars.length; i++) {
			u2_32 pos_32 = {kczi_in.regions[i].vars.pos.x, kczi_in.regions[i].vars.pos.y};
			u2_32 size_32 = {kczi_in.regions[i].vars.size.x, kczi_in.regions[i].vars.size.y};
			decode_region_indexes({out_img, pos_32, size_32}, kczi_in.regions[i], *this);
		}
	}

	return std::move(out_img);
}
auto KcziDec::decode(std::vector<u8> &data, u32 *size_x, u32 *size_y) -> u8 * {
	Bitmap out_img = decode_to_bitmap(data);
	*size_x = out_img.vars.size_x;
	*size_y = out_img.vars.size_y;

	auto return_data = (u8 *)malloc(sizeof(pixel_data) * kczi_in.vars.x * kczi_in.vars.y);
	memcpy(return_data, out_img.data.get(), sizeof(pixel_data) * kczi_in.vars.x * kczi_in.vars.y);
	return return_data;
}
