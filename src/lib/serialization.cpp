#include "serialization.hpp"
#include "bitmap.hpp"
#include <SDL2/SDL_config.h>
#include <cstdio>
#include <kczi_enc.hpp>
#include <utils.hpp>
#include <vector>

#include <zstd.h>

DeserializedKczi::DeserializedKczi() {}
auto DeserializedKczi::deserialize(std::vector<u8> &serialized_data) -> u16 {
	if (serialized_data.size() < 32) [[unlikely]] {
		vars.integrity = vars.integrity | file_errors::INVALID_SIZE;
		return vars.integrity;
	}
	u64 offset = 8;
	u64 filesize = read_stream_element<u64>(serialized_data, offset);
	u64 decopressed_size = read_stream_element<u64>(serialized_data, offset);
	if (serialized_data.size() != filesize || decopressed_size > max_decompressed_size || filesize > max_decompressed_size) [[unlikely]] {
		vars.integrity = vars.integrity | file_errors::INVALID_SIZE;
		return vars.integrity;
	}
	std::vector<u8> dezstd(decopressed_size);
	u64 zstd_out = ZSTD_decompress(dezstd.data(), dezstd.size(), serialized_data.data() + 24, serialized_data.size() - 24);
	if (ZSTD_isError(zstd_out)) [[unlikely]] {
		vars.integrity = vars.integrity | file_errors::INVALID_ZSTD;
		fprintf(stderr, "ZSTD: %s \n", ZSTD_getErrorName(zstd_out));
		return vars.integrity;
	}
	if (22 >= dezstd.size()) [[unlikely]] {
		vars.integrity = vars.integrity | file_errors::INVALID_SIZE;
		return vars.integrity;
	}
	offset = 0;
	vars.x = read_stream_element<u16>(dezstd, offset);
	vars.y = read_stream_element<u16>(dezstd, offset);
	vars.format = read_stream_element<pixel_format>(dezstd, offset);
	vars.max_depth = read_stream_element<u8>(dezstd, offset);
	vars.block_size = read_stream_element<u16>(dezstd, offset);
	vars.color_quant = read_stream_element<u16>(dezstd, offset);
	vars.color_sensitivity = read_stream_element<u16>(dezstd, offset);
	vars.length = read_stream_element<u32>(dezstd, offset);
	vars.dicts_length = read_stream_element<u32>(dezstd, offset);
	vars.dct_start = read_stream_element<u16>(dezstd, offset);
	vars.dct_end = read_stream_element<u16>(dezstd, offset);
	if (vars.length > max_regions || vars.length == 0) [[unlikely]] {
		vars.integrity = vars.integrity | INVALID_REGION_OR_DICT_NUMBER;
		return vars.integrity;
	}
	regions.resize(vars.length);
	for (u32 i = 0; i < vars.length; i++) {
		if (offset + 19 >= dezstd.size()) [[unlikely]] {
			vars.integrity = vars.integrity | file_errors::INVALID_SIZE;
			return vars.integrity;
		}
		auto &r = regions[i];
		r.vars.pos.x = read_stream_element<u16>(dezstd, offset);
		r.vars.pos.y = read_stream_element<u16>(dezstd, offset);
		r.vars.size.x = read_stream_element<u16>(dezstd, offset);
		r.vars.size.y = read_stream_element<u16>(dezstd, offset);

		r.vars.depth = read_stream_element<u8>(dezstd, offset);
		r.vars.dict_index = read_stream_element<u16>(dezstd, offset);
		u32 pixels_size = read_stream_element<u32>(dezstd, offset);
		if (r.vars.pos.x + r.vars.size.x > vars.x ||
				r.vars.pos.y + r.vars.size.y > vars.y ||
				r.vars.dict_index > vars.dicts_length ||
				offset + pixels_size >= dezstd.size()) [[unlikely]] {
			vars.integrity = vars.integrity | file_errors::INVALID_SIZE;
			return vars.integrity;
		}
		read_stream_array<u8>(dezstd, r.pixels, offset, pixels_size);
		u32 algo_size = read_stream_element<u32>(dezstd, offset);
		if (offset + algo_size >= dezstd.size()) [[unlikely]] {
			vars.integrity = vars.integrity | file_errors::INVALID_SIZE;
			return vars.integrity;
		}
		read_stream_array<u8>(dezstd, r.blokcs, offset, algo_size);
	}
	if (vars.format != pixel_format::YUV) {
		if (vars.dicts_length > max_regions) [[unlikely]] {
			vars.integrity =
					vars.integrity | file_errors::INVALID_REGION_OR_DICT_NUMBER;
			return vars.integrity;
		}
		dicts.resize(vars.dicts_length);
		for (u32 i = 0; i < vars.dicts_length; i++) {
			auto &d = dicts[i];
			d.size = read_stream_element<u8>(dezstd, offset);
			if (vars.format == pixel_format::DICTRGB) {
				for (u32 j = 0; j < d.size; j++) {
					pixel_data px;
					px.rgba.r = read_stream_element<u8>(dezstd, offset);
					px.rgba.g = read_stream_element<u8>(dezstd, offset);
					px.rgba.b = read_stream_element<u8>(dezstd, offset);
					px.rgba.a = 255;
					d.entery[j] = px;
				}
			} else {
				read_stream_array<pixel_data>(dezstd, d.entery.data(), offset, d.size);
			}
		}
	}
	return vars.integrity;
}
auto DeserializedKczi::serialize() -> std::vector<u8> {
	u64 expected_size =
			sizeof(vars.x) + sizeof(vars.y) + sizeof(vars.length) +
			sizeof(vars.format) + sizeof(vars.block_size) + sizeof(vars.max_depth) +
			sizeof(vars.color_quant) + sizeof(vars.color_sensitivity) +
			sizeof(vars.dicts_length) + sizeof(vars.dct_end) +
			sizeof(vars.dct_start) +
			(sizeof(Region::vars.pos) + sizeof(Region::vars.size) +
			 sizeof(u32 /*blocks size */) + sizeof(u32 /*pixels size */) +
			 sizeof(Region::vars.dict_index) + sizeof(Region::vars.depth)) *
					vars.length +
			sizeof(u8 /*dcit_size*/) * vars.dicts_length;
	for (auto &r : regions) {
		expected_size += r.blokcs.size();
		expected_size += r.pixels.size();
	}
	for (auto &d : dicts) {
		if (vars.format == pixel_format::DICTRGB)
			expected_size += d.size * 3;
		else
			expected_size += d.size * 4;
	}
	std::vector<u8> data(expected_size);
	u64 offset = 0;
	save_to_vec<u16>(vars.x, data, offset);
	save_to_vec<u16>(vars.y, data, offset);
	save_to_vec<u8>((u8)vars.format, data, offset);
	save_to_vec<u8>(vars.max_depth, data, offset);
	save_to_vec<u16>(vars.block_size, data, offset);
	save_to_vec<u16>(vars.color_quant, data, offset);
	save_to_vec<u16>(vars.color_sensitivity, data, offset);
	save_to_vec<u32>(vars.length, data, offset);
	save_to_vec<u32>(vars.dicts_length, data, offset);
	save_to_vec<u16>(vars.dct_start, data, offset);
	save_to_vec<u16>(vars.dct_end, data, offset);
	for (auto &r : regions) {
		save_to_vec<u2_16>(r.vars.pos, data, offset);
		save_to_vec<u2_16>(r.vars.size, data, offset);
		save_to_vec<u8>(r.vars.depth, data, offset);
		save_to_vec<u16>(r.vars.dict_index, data, offset);
		save_to_vec<u32>((u32)r.pixels.size(), data, offset);
		copy_vec(data, r.pixels, offset);
		save_to_vec<u32>((u32)r.blokcs.size(), data, offset);
		copy_vec(data, r.blokcs, offset);
	}
	if (vars.format == pixel_format::DICTRGBA)
		for (auto &d : dicts) {
			save_to_vec<u8>(d.size, data, offset);
			for (u32 i = 0; i < d.size; i++) {
				save_to_vec<u32>(d.entery[i].data, data, offset);
			}
		}
	else if (vars.format == pixel_format::DICTRGB)
		for (auto &d : dicts) {
			save_to_vec<u8>(d.size, data, offset);
			for (u32 i = 0; i < d.size; i++) {
				save_to_vec<u8>(d.entery[i].rgba.r, data, offset);
				save_to_vec<u8>(d.entery[i].rgba.g, data, offset);
				save_to_vec<u8>(d.entery[i].rgba.b, data, offset);
			}
		}
	std::vector<u8> commpressed(expected_size + 24);
	u64 out_size = ZSTD_compress(commpressed.data() + 24, commpressed.size() - 24, data.data(), data.size(), 20);
	if (ZSTD_isError(out_size)) [[unlikely]] {
		vars.integrity = vars.integrity | file_errors::INVALID_ZSTD;
		return std::vector<u8>();
	}

	commpressed.resize(out_size + 24);
	offset = 8;
	for (u32 i = 0; i < sizeof(header); i++) {
		commpressed[i] = header[i];
	}
	save_to_vec<u64>(commpressed.size(), commpressed, offset);
	save_to_vec<u64>(data.size(), commpressed, offset);

	return commpressed;
}
DeserializedKczi::DeserializedKczi(DeserializedKczi &&d) : regions(std::move(d.regions)), vars(d.vars), dicts(std::move(d.dicts)) {}
DeserializedKczi::~DeserializedKczi() {}
Dict::Dict() {}
Dict::~Dict() {}
