#pragma once
#include <array>
#include <bitmap.hpp>
#include <region.hpp>
#include <utils.hpp>
#include <vector>
constexpr u32 max_regions = 4096 * 4096;
constexpr u32 max_decompressed_size = (u32)1024 * 1024 * 1024 * 2;
constexpr u8 header[8] = {'K', 'C', 'Z', 'I',
													'\x00', '\x00', '\x00', '\x07'};
enum file_errors : u16 {
	OK,
	INVALID_SIZE = 0x1,
	INVALID_ZSTD = 0x2,
	INVALID_REGION_OR_DICT_NUMBER = 0x4,
	INVLID_DICT_LEN = 0x8,
};

class Dict {
public:
	Dict();
	~Dict();
	u16 size;
	std::array<pixel_data, 256> entery;
};
class DeserializedKczi {
public:
	DeserializedKczi();
	DeserializedKczi(DeserializedKczi &&d);
	auto deserialize(std::vector<u8> &serialized_data) -> u16;
	auto serialize() -> std::vector<u8>;
	~DeserializedKczi();
	struct {
		u32 x;
		u32 y;
		pixel_format format;
		u8 max_depth;
		u16 block_size;
		u16 color_quant;
		u16 color_sensitivity;
		u32 length;
		u32 dicts_length;
		u16 dct_start;
		u16 dct_end;
		u32 avg_edges;
		u16 integrity = 0;
	} vars;
	std::vector<Region> regions;
	std::vector<Dict> dicts;
};