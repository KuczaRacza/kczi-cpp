#include "utils.hpp"
#include <array>
#include <exception>
#include <fstream>
#include <ios>
#include <vector>
auto read_file(std::string filename, std::vector<u8> &out) -> void {
  std::ifstream f;
  f.open(filename, std::ios::binary | std::ios::in);
  if (!f.good()) {
    throw std::exception();
  }
  f.seekg(0, std::ios::end);
  u64 size = f.tellg();
  f.seekg(0, std::ios::beg);
  out.resize(size);
  f.read((char *)out.data(), size);
}
