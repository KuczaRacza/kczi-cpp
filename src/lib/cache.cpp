#include <cache.hpp>
#include <utility>
#include <yuv_blocks.hpp>
Quant_matrix_dct::Quant_matrix_dct(u64 size) : std::vector<u16>(size) {
}

auto Cache::get_q_matrix(u2_32 size) -> const Quant_matrix_dct & {
	auto it = dct_q_cache.find(size);
	if (it == dct_q_cache.end()) {
		it = dct_q_cache.emplace(std::make_pair(size, std::move(Quant_matrix_dct(size.get_product())))).first;
		dct_create_matrix(it->second, img_dct_quant.x, img_dct_quant.y, size);
	}
	return it->second;
}
