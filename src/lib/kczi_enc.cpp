#include "kczi_enc.hpp"
#include "bitmap.hpp"
#include "region.hpp"
#include "utils.hpp"
#include <algorithm>
#include <array>
#include <blocks.hpp>
#include <cstdio>
#include <edges.hpp>
#include <functional>
#include <iterator>
#include <serialization.hpp>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>
#include <yuv_blocks.hpp>
#include <yuv_conversion.hpp>
KcziEnc::KcziEnc() {}
KcziEnc::~KcziEnc() {}
template <u8 channels = 4>
static inline auto color_reduction(BitmapRef bmap, u32 quant) -> void {
	bmap.for_pixel([=](pixel_data &px) {
		for (u32 c = 0; c < channels; c++) {
			px.channels[c] = (px.channels[c] / quant) * quant;
		}
	});
}
auto KcziEnc::rect_tree_indexed(BitmapRef bmap) -> u8 {
	regions.reserve((bmap.size / ty2<u32>(16, 16)).get_product());
	recur_rect_tree_indexed(bmap, 0);
	u8 max_depth =
			std::min_element(regions.begin(), regions.end(),
											 [](const UnencRegion &a, const UnencRegion &b) {
												 return a.vars.depth > b.vars.depth;
											 })
					->vars.depth;
	max_depth = std::max<u8>(1, max_depth);
	return max_depth;
}
auto KcziEnc::encode_indexed(Bitmap &b, enc_args args, DeserializedKczi &kczi)
		-> void {
	BitmapRef main_ref = {b, {0, 0}, {b.vars.size_x, b.vars.size_y}};
	if (args.color_quant > 1) {
		if (args.alpha) {
			color_reduction(main_ref, args.color_sensitivity);
		} else {
			color_reduction<3>(main_ref, args.color_sensitivity);
		}
	}
	auto max_depth = rect_tree_indexed(main_ref);
	dicts.resize(regions.size());
	for (u64 id = 0; auto &reg : regions) {
		auto &dict_it = dicts[dict_written];
		region_enc(reg, dict_it, args, max_depth, *this, id);

		id++;
	}
	kczi.dicts.swap(dicts);
	kczi.dicts.resize(dict_written);
	kczi.vars.max_depth = max_depth;
}
static auto recur_rect_tree_yuv(std::vector<UnencRegion> &regs, BitmapRef b, u8 depth, MultiBitmapRef<1, u16> edges) -> void {
	constexpr u32 max_diffrence = 360;
	constexpr u32 min_noissness = 65535;
	if (b.size.x < 72 || b.size.y < 72) {
		regs.emplace_back(b, depth);
		return;
	}
	BitmapRef first(b);
	BitmapRef second(b);
	if (depth % 2) {
		first.pos = b.pos;
		first.size = {b.size.x / 2, b.size.y};
		second.pos = {b.pos.x + b.size.x / 2, b.pos.y};
		second.size = {b.size.x - b.size.x / 2, b.size.y};
	} else {
		first.pos = b.pos;
		first.size = {b.size.x, b.size.y / 2};
		second.pos = {b.pos.x, b.pos.y + b.size.y / 2};
		second.size = {b.size.x, b.size.y - b.size.y / 2};
	}
	auto first_edges = MultiBitmapRef(edges.map, first.pos, first.size);
	auto second_edges = MultiBitmapRef(edges.map, second.pos, second.size);
	u64 first_edges_sum = 0;
	u64 second_edges_sum = 0;
	first_edges.for_pixel([&](auto px) { first_edges_sum += (px[0] * px[0]) / 8; });
	second_edges.for_pixel([&](auto px) { second_edges_sum += (px[0] * px[0]) / 8; });
	first_edges_sum = first_edges_sum * 16 / first_edges.size.get_product();
	second_edges_sum = second_edges_sum * 16 / second_edges.size.get_product();

	u32 diffrence = (first_edges_sum > second_edges_sum) ? first_edges_sum - second_edges_sum : second_edges_sum - first_edges_sum;
	if (diffrence > max_diffrence && first_edges_sum > min_noissness) {
		recur_rect_tree_yuv(regs, first, depth + 1, first_edges);
		recur_rect_tree_yuv(regs, second, depth + 1, second_edges);
	} else {
		regs.emplace_back(b, depth);
	}
}
static inline auto rect_tree_yuv(std::vector<UnencRegion> &regs, BitmapRef b, MultiChannelBitmap<1, u16> &edges) -> u8 {
	regs.reserve((b.size / ty2<u32>(16, 16)).get_product());
	recur_rect_tree_yuv(regs, b, 0, {edges, {0, 0}, edges.vars.size});
	return std::max_element(regs.begin(), regs.end(), [](const UnencRegion &a, const UnencRegion &b) { return a.vars.depth < b.vars.depth; })
			->vars.depth;
}
static inline auto choose_algo_yuv(BitmapRef b, MultiBitmapRef<1, u16> edges, u32 sensivity) -> BLOCK_ALGO_YUV {
	/*
	constexpr u32 dct_sensivity = 1024;
	constexpr u32 gradient_sensivity = 2048;
	constexpr u32 sub_sampling = 6;

	std::array<i32, 4> factors;
	u32 mean_edges = 0;
	edges.for_pixel([&](std::array<u16, 1> &px) {
		mean_edges += px[0];
	});
	mean_edges = mean_edges * 256 / edges.size.get_product();
	factors[(u8)BLOCK_ALGO_YUV::GRAD] = sensivity * gradient_sensivity;
	factors[(u8)BLOCK_ALGO_YUV::DCT] = mean_edges * dct_sensivity;
	factors[(u8)BLOCK_ALGO_YUV::PREDICT] = -max_i32;
	factors[(u8)BLOCK_ALGO_YUV::SUBSAMPLED] = -max_i32;
	return (BLOCK_ALGO_YUV)std::distance(factors.begin(), std::max_element(factors.begin(), factors.end()));
	*/
	bool grad = true;
	edges.for_pixel([&](std::array<u16, 1> &px) {
		if (px[0] > 256) {
			grad = false;
		}
	});
	if (grad) {
		return BLOCK_ALGO_YUV::GRAD;
	} else {
		return BLOCK_ALGO_YUV::DCT;
	}
}
template <bool alpha>
static inline auto encode_region_yuv(UnencRegion &reg, KcziEnc::enc_args args, u8 max_depth, MultiBitmapRef<1, u16> edges, Cache &cache) {
	u32 block_size = (1.0f - reg.vars.depth / (float)max_depth) * args.block_size;
	block_size = (block_size < 8) ? 8 : block_size;
	block_size /= 2;
	block_size *= 2;
	u32 offset = 0;
	u2_32 blocks_number = reg.b.size / ty2(block_size, block_size);
	blocks_number.x = std::max<u32>(1, blocks_number.x);
	blocks_number.y = std::max<u32>(1, blocks_number.y);
	reg.pixels.reserve(reg.blokcs.size() * 2 * (3 + alpha));
	reg.blokcs.resize(blocks_number.get_product() / 4 + (blocks_number.get_product() % 4 != 0));
	auto per_block = [&](u2_32 coord, BitmapRef ref, u2_32 size_blocks) {
		u16 bitpos = coord.x + size_blocks.x * coord.y;
		u16 bitshift = (bitpos % 4 * 2);
		auto algo = choose_algo_yuv(
				ref, MultiBitmapRef<1, u16>(edges.map, ref.pos, ref.size),
				args.color_sensitivity);
		reg.blokcs[bitpos / 4] = reg.blokcs[bitpos / 4] | ((u8)algo << bitshift);
		if (algo == BLOCK_ALGO_YUV::SUBSAMPLED) {
			yuv_subsample_enc<alpha>(reg.pixels, ref);
		} else if (algo == BLOCK_ALGO_YUV::GRAD) {
			yuv_grad_enc<alpha>(reg.pixels, ref);
		} else if (algo == BLOCK_ALGO_YUV::DCT) {
			if constexpr (alpha) {
				yuv_dct_alpha_enc(reg.pixels, ref, cache);
			} else {
				yuv_dct_enc(reg.pixels, ref, cache);
			}
		}
	};
	reg.loop(block_size, per_block, reg.b);
	reg.vars.dict_index = 0;
}
auto KcziEnc::encode_yuv(Bitmap &bmap, enc_args args, DeserializedKczi &kczi) -> void {
	BitmapRef main_ref = {bmap, {0, 0}, {bmap.vars.size_x, bmap.vars.size_y}};
	rgb_to_yuv(main_ref);
	auto image_edges = create_edges_map(main_ref);
	//image_edges.save_bmp("abc.bmp");
	u8 max_depth = rect_tree_yuv(regions, main_ref, image_edges);
	kczi.vars.max_depth = max_depth;
	if (kczi.vars.format == pixel_format::YUVA) {
		for (auto &reg : regions) {
			encode_region_yuv<true>(reg, args, max_depth, MultiBitmapRef(image_edges, reg.b.pos, reg.b.size), cache);
		}
	} else {
		for (auto &reg : regions) {
			encode_region_yuv<false>(reg, args, max_depth, MultiBitmapRef(image_edges, reg.b.pos, reg.b.size), cache);
		}
	}
}
auto KcziEnc::encode(Bitmap &&bmap, enc_args args) -> std::vector<u8> {
	Bitmap b(std::move(bmap));
	DeserializedKczi k;
	cache.img_dct_quant = {args.dct_start, args.dct_end};
	if (args.mode == enc_args::enc_mode::yuv) {
		k.vars.format = (args.alpha) ? pixel_format::YUVA : pixel_format::YUV;
		encode_yuv(b, args, k);
	} else {
		encode_indexed(b, args, k);
		k.vars.format =
				(args.alpha) ? pixel_format::DICTRGBA : pixel_format::DICTRGB;
	}
	k.regions.reserve(regions.size());
	for (auto &reg : regions) {
		k.regions.emplace_back(std::move(reg));
	}
	k.vars.x = b.vars.size_x;
	k.vars.y = b.vars.size_y;
	k.vars.block_size = args.block_size;
	k.vars.color_quant = args.color_quant;

	k.vars.color_sensitivity = args.color_sensitivity;
	k.vars.dct_end = args.dct_end;
	k.vars.dct_start = args.dct_start;
	k.vars.length = k.regions.size();
	k.vars.dicts_length = k.dicts.size();
	return k.serialize();
}
[[gnu::always_inline, gnu::hot]] static inline auto
merge_dicts(Dict &src, Dict &dst, std::vector<u8> &src_pixels) -> bool {
	std::unordered_map<u32, u8> src_data(255);
	std::array<std::pair<u8, u8>, 256> exchanges;
	u32 exchanges_number = 0;
	for (u32 i = 0; i < src.size; i++) {
		src_data.emplace(std::make_pair(src.entery[i].data, i));
	}
	for (u32 i = 0; i < dst.size; i++) {
		auto it = src_data.find(dst.entery[i].data);
		if (it != src_data.end()) {
			exchanges[exchanges_number] = std::make_pair(i, it->second);
			exchanges_number++;
			src_data.erase(it);
		}
	}

	if (src_data.size() + dst.size > 255) {
		return false;
	}
	for (auto &p : src_data) {
		dst.entery[dst.size].data = p.first;
		exchanges[exchanges_number] = std::make_pair(dst.size, p.second);
		dst.size++;
		exchanges_number++;
	}
	for (u8 &px : src_pixels) {
		for (u32 i = 0; i < exchanges_number; i++) {
			if (px == exchanges[i].second) {
				px = exchanges[i].first;
				break;
			}
		}
	}
	src.size = 0;
	return true;
}
static inline auto count_colors(BitmapRef b, u32 limit) -> u32 {
	std::unordered_set<u32> lookup(256);
	for (u32 y = 0; y < b.size.y; y++) {
		for (u32 x = 0; x < b.size.x; x++) {
			pixel_data px;
			px.data = b.get_pixel({x, y});
			if (lookup.find(px.data) == lookup.end()) {
				lookup.emplace(px.data);
				if (lookup.size() > limit) {
					return max_u32;
				}
			}
		}
	}

	return lookup.size();
}
auto KcziEnc::recur_rect_tree_indexed(BitmapRef bmap, u8 depth) -> void {
	if (bmap.size.x < 8 || bmap.size.y < 8) [[unlikely]] {
		regions.push_back({bmap, depth});
	} else if (count_colors(bmap, 255) == max_u32) {
		BitmapRef first(bmap);
		BitmapRef second(bmap);
		if (depth % 2) {
			first.pos = bmap.pos;
			first.size = {bmap.size.x / 2, bmap.size.y};
			second.pos = {bmap.pos.x + bmap.size.x / 2, bmap.pos.y};
			second.size = {bmap.size.x - bmap.size.x / 2, bmap.size.y};
		} else {
			first.pos = bmap.pos;
			first.size = {bmap.size.x, bmap.size.y / 2};
			second.pos = {bmap.pos.x, bmap.pos.y + bmap.size.y / 2};
			second.size = {bmap.size.x, bmap.size.y - bmap.size.y / 2};
		}

		recur_rect_tree_indexed(first, depth + 1);
		recur_rect_tree_indexed(second, depth + 1);

	} else {
		regions.emplace_back(bmap, depth);
	}
}
static inline auto try_merge_fast(std::vector<Dict> &dicts, std::vector<u8> &pixles, u16 &dict_index, Dict &d, u32 &dict_written) -> void {
	for (i32 i = dict_written - 1; i >= 0 && dict_written - i < 4; i--) {
#ifdef LOGS
		printf("merging  %u and  %i \n", dict_written, i);
#endif
		if (merge_dicts(d, dicts[i], pixles)) {
#ifdef LOGS
			printf("SUCCESS\n");
#endif
			dict_index = i;
			return;
		}
	}
	dict_index = dict_written;
	dict_written++;
}

static inline auto try_merge(std::vector<Dict> &dicts, std::vector<u8> &pixles,
														 u16 &dict_index, Dict &d, u32 &dict_written)
		-> void {
	for (i32 i = dict_written - 1; i >= 0 && dict_written - i < 64; i--) {
#ifdef LOGS
		printf("merging  %u and  %i \n", dict_written, i);
#endif
		if (merge_dicts(d, dicts[i], pixles)) {
#ifdef LOGS
			printf("SUCCESS\n");
#endif
			dict_index = i;
			return;
		}
	}
	dict_index = dict_written;
	dict_written++;
}
static auto index_colors(std::vector<u32> &in, Dict &out) -> std::vector<u8> {
	u16 lookup_size = 0;
	std::unordered_map<u32, u8> lookup;
	std::vector<u8> indx;
	indx.reserve(in.size());
	for (auto &c : in) {
		auto lookup_ref = lookup.find(c);
		if (lookup_ref == lookup.end()) {
			lookup.insert(std::pair<u32, u8>(c, lookup_size));
			lookup_size++;
			indx.push_back(lookup_size - 1);

		} else {
			indx.push_back(lookup_ref->second);
		}
	}
	for (auto &p : lookup) {
		pixel_data px;
		px.data = p.first;
		out.entery[p.second] = px;
	}
	out.size = lookup_size;
	return indx;
}
[[nodiscard]] static auto choose_algo_indexed(BitmapRef block, u32 thereshold)
		-> BLOCK_ALGO_INX {
	std::array<u32, 4> max;
	max.fill(0);
	std::array<u32, 4> min;
	min.fill(max_u32);
	std::array<u32, 4> diffs;
	block.for_pixel([&](pixel_data &px) {
		for (u32 c = 0; c < 4; c++) {
			if (px.channels[c] > max[c]) {
				max[c] = px.channels[c];
			}
			if (px.channels[c] < min[c]) {
				min[c] = px.channels[c];
			}
		}
	});
	for (u32 c = 0; c < 4; c++) {
		diffs[c] = max[c] - min[c];
	}
	u32 bl_diff = *std::max_element(diffs.begin(), diffs.end());
	if (bl_diff > thereshold) {
		return BLOCK_ALGO_INX::FULL_RES;
	} else {
		return BLOCK_ALGO_INX::GRAD;
	}
}

auto KcziEnc::region_enc(UnencRegion &reg, Dict &dict, enc_args e_args, u8 max_depth, KcziEnc &enc, u32 id) -> void {
	u32 block_size = (1.0f - reg.vars.depth / (float)max_depth) * e_args.block_size;
	block_size = (block_size < 8) ? 8 : block_size;
	block_size /= 2;
	block_size *= 2;
	u32 offset = 0;
	u2_32 blocks_number = reg.b.size / ty2<u32>(block_size, block_size);
	blocks_number.x = std::max<u32>(1, blocks_number.x);
	blocks_number.y = std::max<u32>(1, blocks_number.y);
	std::vector<u32> unindexed;
	unindexed.reserve(reg.b.size.get_product());
	reg.blokcs.resize(blocks_number.get_product() / 4 + (blocks_number.get_product() % 4 != 0));

	auto per_block = [&](u2_32 coord, BitmapRef ref, u2_32 size_blocks) {
		u16 bitpos = coord.x + size_blocks.x * coord.y;
		u16 bitshift = (bitpos % 4 * 2);
		auto algo = choose_algo_indexed(ref, e_args.color_sensitivity);
		reg.blokcs[bitpos / 4] = reg.blokcs[bitpos / 4] | ((u8)algo << bitshift);
		if (algo == BLOCK_ALGO_INX::FULL_RES) {
			enc_indx_full(ref, unindexed);

		} else if (algo == BLOCK_ALGO_INX::GRAD) {
			enc_indx_gradient(ref, unindexed);
		}
	};
	reg.loop(block_size, per_block, reg.b);
	reg.pixels = index_colors(unindexed, enc.dicts[enc.dict_written]);
	if (id == 0) [[unlikely]] {
		reg.vars.dict_index = enc.dict_written;
		enc.dict_written++;
	} else {
		try_merge_fast(enc.dicts, reg.pixels, reg.vars.dict_index, dict, enc.dict_written);
	}
}
