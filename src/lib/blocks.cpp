#include "blocks.hpp"
#include "utils.hpp"
#include <algorithm>
#include <alloca.h>
#include <array>
#include <bitmap.hpp>
#include <cstddef>
#include <initializer_list>
#include <math.h>
#include <span>
#include <stdio.h>
#include <string_view>
auto indexed_full_blocks(const u8 *data, const Dict &d, BitmapRef ret) -> u32 {
	for (u32 i = 0; i < ret.size.y; i++) {
		for (u32 j = 0; j < ret.size.x; j++) {
			ret.set_pixel({j, i}, d.entery[data[i * ret.size.x + j]].data);
		}
	}
	return ret.size.get_product();
}

auto indexed_interpolation_dynamic(const std::basic_string_view<u8> data, const Dict &d, BitmapRef ret) -> u32 {
	std::array<pixel_data, 4> corners;

	corners[0] = d.entery[data[0]];
	corners[1] = d.entery[data[1]];
	corners[2] = d.entery[data[2]];
	corners[3] = d.entery[data[3]];

	for (u32 y = 0; y < ret.size.y; y++) {
		for (u32 x = 0; x < ret.size.x; x++) {
			pixel_data px;
			std::array<u32, 4> colors;
			colors.fill(0);
			std::array<u32, 4> interpolation_corners;

			interpolation_corners[0] = ((ret.size.x - x) * max_u16 / ret.size.x) * (ret.size.y - y) / ret.size.y;
			interpolation_corners[1] = (x * max_u16 / ret.size.x) * (ret.size.y - y) / ret.size.y;
			interpolation_corners[2] = (x * max_u16 / ret.size.x) * y / ret.size.y;
			interpolation_corners[3] = ((ret.size.x - x) * max_u16 / ret.size.x) * y / ret.size.y;

			for (u8 k = 0; k < 4; k++) {
				for (u32 c = 0; c < 4; c++) {
					colors[c] += interpolation_corners[k] * corners[k].channels[c];
				}
			}

			for (u32 c = 0; c < 4; c++) {
				u16 roundup = colors[c] & ((max_u16) / 2);
				px.channels[c] = colors[c] / (max_u16);
				if (roundup) {
					if (px.channels[c] != 255)
						px.channels[c]++;
				}
			}
			ret.set_pixel({x, y}, px.data);
		}
	}
	return 4;
}
namespace interpolation_static {
template <u2_32 size, u2_32 coord, u8 rot>
inline constexpr auto indexed_itrpl_mul() {
	if constexpr (rot == 0) {
		u32 horizontal = (size.x - coord.x) * max_u16 / size.x;
		u32 vertical = horizontal * (size.y - coord.y) / size.y;
		return vertical;
	} else if constexpr (rot == 1) {
		u32 horizontal = coord.x * max_u16 / size.x;
		u32 vertical = horizontal * (size.y - coord.y) / size.y;
		return vertical;
	} else if constexpr (rot == 2) {
		u32 horizontal = coord.x * max_u16 / size.x;
		u32 vertical = horizontal * coord.y / size.y;
		return vertical;
	} else if constexpr (rot == 3) {
		u32 horizontal = (size.x - coord.x) * max_u16 / size.x;
		u32 vertical = horizontal * coord.y / size.y;
		return vertical;
	}
}
template <u8 rotation, u2_32 size, u2_32 coord>
static inline auto constexpr for_rotation(const std::array<pixel_data, 4> &corners, std::array<u32, 4> &colors) -> void {
	u32 multi = indexed_itrpl_mul<size, coord, rotation>();
	for (u32 c = 0; c < 4; c++) {
		colors[c] += multi * corners[rotation].channels[c];
	}
	if constexpr (rotation != 0) {
		for_rotation<rotation - 1, size, coord>(corners, colors);
	}
}
template <u2_32 coord, u2_32 size>
[[gnu::always_inline]] static auto constexpr constexprloop(BitmapRef &ret, std::array<pixel_data, 4> corners) -> void {
	pixel_data px;
	std::array<u32, 4> colors;
	colors.fill(0);
	for_rotation<3, size, coord>(corners, colors);
	for (u32 c = 0; c < 4; c++) {
		u16 roundup = colors[c] & ((max_u16) / 2);
		px.channels[c] = colors[c] / (max_u16);
		if (roundup) {
			if (px.channels[c] != 255)
				px.channels[c]++;
		}
	}
	ret.set_pixel(coord, px.data);
	if constexpr (coord.x < size.x) {
		constexprloop<ty2<u32>(coord.x + 1, coord.y), size>(ret, corners);
	} else if constexpr (coord.y < size.y) {
		constexprloop<ty2<u32>(0, coord.y + 1), size>(ret, corners);
	}
}
}; // namespace interpolation_static

template <u2_32 SIZE>
auto indexed_interpolation_static(const std::basic_string_view<u8> data, const Dict &d, BitmapRef ret) -> u32 {
	std::array<pixel_data, 4> corners;
	corners[0] = d.entery[data[0]];
	corners[1] = d.entery[data[1]];
	corners[2] = d.entery[data[2]];
	corners[3] = d.entery[data[3]];
	interpolation_static::constexprloop<ty2<u32>(0, 0), SIZE>(ret, corners);
	return 4;
}

auto indexed_interpolation(const std::basic_string_view<u8> data, const Dict &d, BitmapRef ret) -> u32 {

	if (std::equal(data.begin(), data.end(), data.rbegin())) {
		pixel_data my_px = d.entery[data[0]];
		ret.for_pixel([&](pixel_data &px) { px = my_px; });
	}
	if (ret.size.x != ret.size.y) {
		return indexed_interpolation_dynamic(data, d, ret);
	}
/*
	if (ret.size.x == 8) {
		constexpr u2_32 size = {8, 8};
		return indexed_interpolation_static<size>(data, d, ret);
#ifdef NDEBUG
	} else if (ret.size.x == 10) {
		constexpr u2_32 size = {10, 10};
		return indexed_interpolation_static<size>(data, d, ret);
	} else if (ret.size.x == 12) {
		constexpr u2_32 size = {12, 12};
		return indexed_interpolation_static<size>(data, d, ret);
	} else if (ret.size.x == 14) {
		constexpr u2_32 size = {14, 14};
		return indexed_interpolation_static<size>(data, d, ret);
	} else if (ret.size.x == 16) {
		constexpr u2_32 size = {16, 16};
		return indexed_interpolation_static<size>(data, d, ret);
#endif
	} else {
		*/
		return indexed_interpolation_dynamic(data, d, ret);
//	}
}
