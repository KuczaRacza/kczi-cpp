#pragma once
#include <alloca.h>
#include <array>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <stdint.h>
#include <string>
#include <vector>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
constexpr i8 max_i8 = INT8_MAX;
constexpr i16 max_i16 = INT16_MAX;
constexpr i32 max_i32 = INT32_MAX;
constexpr i64 max_i64 = INT64_MAX;
constexpr u8 max_u8 = UINT8_MAX;
constexpr u16 max_u16 = UINT16_MAX;
constexpr u32 max_u32 = UINT32_MAX;
constexpr u64 max_u64 = UINT64_MAX;

struct rgba_pixel {
	u8 r;
	u8 g;
	u8 b;
	u8 a;
	inline auto operator+(const rgba_pixel &p) -> rgba_pixel const {
		rgba_pixel tmp;
		tmp.r = p.r + r;
		tmp.g = p.g + g;
		tmp.b = p.b + b;
		tmp.a = p.a + a;
		return tmp;
	}
	inline auto operator-(const rgba_pixel &p) -> rgba_pixel const {
		rgba_pixel tmp;
		tmp.r = r - p.r;
		tmp.g = g - p.g;
		tmp.b = b - p.b;
		tmp.a = a - p.a;
		return tmp;
	}
	inline auto operator==(const rgba_pixel &p) -> bool {
		if (r != p.r)
			return false;
		if (g != p.g)
			return false;
		if (b != p.b)
			return false;
		if (a != p.a)
			return false;
		return true;
	}
};
struct yuva_pixel {
	u8 y;
	u8 u;
	u8 v;
	u8 a;
	inline auto operator+(const yuva_pixel &p) -> yuva_pixel const {
		yuva_pixel tmp;
		tmp.y = p.y + y;
		tmp.u = p.u + u;
		tmp.v = p.v + v;
		tmp.a = p.a + a;
		return tmp;
	}
	inline auto operator-(const yuva_pixel &p) -> yuva_pixel const {
		yuva_pixel tmp;
		tmp.y = y - p.y;
		tmp.u = u - p.u;
		tmp.v = v - p.v;
		tmp.a = a - p.a;
		return tmp;
	}
	inline auto operator==(const yuva_pixel &p) -> bool {
		if (y != p.y)
			return false;
		if (u != p.u)
			return false;
		if (v != p.v)
			return false;
		if (a != p.a)
			return false;
		return true;
	}
};
union pixel_data {
	u32 data;
	u8 channels[4];
	yuva_pixel yuva;
	rgba_pixel rgba;
	inline auto operator==(const pixel_data &p) -> bool { return rgba == p.rgba; }
	inline auto operator-(const pixel_data &p) -> pixel_data const {
		pixel_data tmp;
		tmp.rgba = rgba - p.rgba;
		return tmp;
	}
	inline auto operator+(const pixel_data &p) -> pixel_data const {
		pixel_data tmp;
		tmp.rgba = rgba + p.rgba;
		return tmp;
	}
};
struct rect {
	u16 x;
	u16 y;
	u16 w;
	u16 h;
};
template <typename T>
struct type2 {
	T x;
	T y;
	inline type2<T> operator+(const type2<T> &t1) const {
		type2<T> tmp = {x + t1.x, y + t1.y};
		return tmp;
	};
	inline type2<T> operator-(const type2<T> &t1) const {
		type2<T> tmp = {x - t1.x, y - t1.y};
		return tmp;
	};
	inline type2<T> operator*(const type2<T> &t1) const {
		type2<T> tmp = {x * t1.x, y * t1.y};
		return tmp;
	};
	inline type2<T> operator/(const type2<T> &t1) const {
		type2<T> tmp = {x / t1.x, y / t1.y};
		return tmp;
	};
	inline type2<T> operator%(const type2<T> &t1) const {
		type2<T> tmp = {x % t1.x, y % t1.y};
		return tmp;
	};
	constexpr inline T get_product() const noexcept { return x * y; }
	constexpr inline T get_ratio() const { return x / y; }
	constexpr inline T get_reverse_ratio() { return y / x; }
	constexpr inline bool operator==(const type2<T> &t) const {
		return (t.x == x) && (t.y == y);
	}
};
template <typename T, typename Y>
inline type2<T> type2_cast(type2<Y> t2) {
	type2<T> ret = {(T)(t2.x), (T)(t2.y)};
	return ret;
}
template <typename T>
bool is_coliding(type2<T> point, type2<T> pos, type2<T> end) {
	if (point.x > pos.x && point.x < end.x && point.y > pos.y && point.y < end.y) {
		return true;
	}
	return false;
}
template <typename T>
constexpr inline auto ty2(T x, T y) {
	return type2<T>({x, y});
}
typedef type2<int32_t> i2_32;
typedef type2<int64_t> i2_64;
typedef type2<int16_t> i2_16;
typedef type2<uint64_t> u2_64;
typedef type2<uint16_t> u2_16;
typedef type2<uint32_t> u2_32;
#if !(defined(__clang__) && defined(__CUDA__))
typedef type2<float> float2;
typedef type2<double> double2;
#endif
template <>
struct std::hash<u2_32> {
	std::size_t operator()(u2_32 const &s) const noexcept {
		std::size_t h1 = std::hash<int>{}(s.x);
		std::size_t h2 = std::hash<int>{}(s.y);
		return h1 ^ (h2 << 1);
	}
};
[[gnu::pure, gnu::always_inline]] inline auto read_bits(u8 byte) -> std::array<u8, 8> {
	std::array<u8, 8> bits;
	bits.fill(0);
	for (u8 i = 0; i < 8; i++) {
		bits[i] = (byte << i) & 1;
	}
	return bits;
}
template <typename T>
[[gnu::always_inline, nodiscard]] inline auto
read_stream_element(std::vector<u8> &vec, u64 &offset) -> T {

	T out;
	memcpy(&out, (u8 *)vec.data() + offset, sizeof(T));
	offset += sizeof(T);
	return out;
}
template <typename T, typename V = u8>
[[gnu::always_inline]] inline auto
read_stream_array(std::vector<V> &vec, std::vector<T> &out, u64 &offset,
									u64 number_e = 1) -> void {
	/*
out.resize(number_e);
for (u32 i = 0; i < number_e; i++) {
V *addr = &vec[offset / sizeof(V)];
out[i] = *(T *)addr;
offset += sizeof(T);
}*/
	out.resize(number_e);
	memcpy((u8 *)out.data(), (u8 *)vec.data() + offset, number_e * sizeof(T));
	offset += number_e * sizeof(T);
}
template <typename T, typename V = u8>
[[gnu::always_inline]] inline auto read_stream_array(std::vector<V> &vec,
																										 T *data, u64 &offset,
																										 u64 number_e) -> void {

	memcpy(data, ((u8 *)vec.data()) + offset, sizeof(T) * number_e);
	offset += sizeof(T) * number_e;
}
auto read_file(std::string filename, std::vector<u8> &out) -> void;
template <typename T, typename Y = u8, u64 elements = 1>
inline auto save_to_vec(T data, std::vector<Y> &out, u64 &offset) -> void {
	/*
constexpr u32 repeat = sizeof(T) * elements / sizeof(Y) +
									 (sizeof(T) * elements / sizeof(Y) == 0);
u32 element_offset = offset / sizeof(Y);
for (u32 i = 0; i < repeat; i++) {
out[element_offset + i] = *(((Y *)&data) + i);
}
*/
	memcpy((u8 *)out.data() + offset, &data, sizeof(data));
	offset += sizeof(T);
}
template <typename T>
inline auto copy_vec(std::vector<T> &dst, std::vector<T> &src, u64 &dst_off) {
	memcpy((u8 *)dst.data() + dst_off, (u8 *)src.data(), sizeof(T) * src.size());
	dst_off += sizeof(T) * src.size();
}
enum class pixel_format : u8 {
	RGBA,
	RGB,
	DICTRGBA,
	DICTRGB,
	YUV,
	YUVA
};
[[gnu::pure]] inline auto get_bpp(pixel_format f) {
	switch (f) {
	case pixel_format::RGBA:
		return 4;
		break;
	case pixel_format::RGB:
		return 3;
		break;
	case pixel_format::DICTRGBA:
		return 1;
		break;
	case pixel_format::DICTRGB:
		return 1;
		break;
	case pixel_format::YUV:
		return 3;
		break;
	case pixel_format::YUVA:
		return 4;
		break;
	}
}
