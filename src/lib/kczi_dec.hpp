#pragma once
#include "cache.hpp"
#include <bitmap.hpp>
#include <serialization.hpp>
#include <utils.hpp>
#include <vector>
class KcziDec {
public:
	KcziDec();
	~KcziDec();
	auto decode(std::vector<u8> &data, u32 *size_x, u32 *size_y) -> u8 *;
	auto decode_to_bitmap(std::vector<u8> &data) -> Bitmap;
	Bitmap out_img;
	DeserializedKczi kczi_in;
	Cache cache;
	u8 max_depth;
};