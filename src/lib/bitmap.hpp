#pragma once
#include "utils.hpp"
#include <SDL2/SDL.h>
#include <array>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <limits>
#include <memory>
#include <string>
#include <string_view>
#include <utility>
class px_deleter {
public:
	void operator()(pixel_data *px) { delete[] px; }
};
class Bitmap {
public:
	Bitmap();
	~Bitmap();
	Bitmap(Bitmap &) = delete;
	Bitmap(Bitmap &&b);
	struct vars {
		u32 size_x;
		u32 size_y;
	} vars;
	auto load_img(SDL_Surface *surf) -> void;
	auto crate_sdl() -> SDL_Surface *;
	auto save_bmp(std::string_view path) -> void;
	auto crate(u2_32 size) -> void;
	[[gnu::hot, gnu::always_inline]] inline auto set_pixel(u2_32 pos, u32 pixel) -> void {
		(data.get() + vars.size_x * pos.y + pos.x)->data = pixel;
#ifdef DEBUG_DUMP
		save_bmp("debug_log.bmp");
#endif
	}
	[[gnu::hot, gnu::pure, gnu::always_inline]] inline auto get_pixel(u2_32 pos) const -> u32 {
		return (data.get() + vars.size_x * pos.y + pos.x)->data;
	}
	[[gnu::hot, gnu::pure, gnu::always_inline]] inline auto get_pixel_ref(u2_32 pos) -> pixel_data & {
		return *(data.get() + vars.size_x * pos.y + pos.x);
#ifdef DEBUG_DUMP
		save_bmp("debug_log.bmp");
#endif
	}
	std::unique_ptr<pixel_data, px_deleter> data;
};
class BitmapRef {
public:
	BitmapRef(Bitmap &b, u2_32 pos, u2_32 size);
	~BitmapRef();
	[[gnu::hot, gnu::always_inline]] inline auto get_pixel(u2_32 pos) const -> u32 {
		return b.get_pixel(pos + this->pos);
	}
	[[gnu::hot, gnu::always_inline]] inline auto get_pixel_ref(u2_32 pos) -> pixel_data & {
		return b.get_pixel_ref(pos + this->pos);
	}
	[[gnu::hot, gnu::always_inline]] inline auto set_pixel(u2_32 pos, u32 pixel) -> void {

		b.set_pixel(pos + this->pos, pixel);
	}
	template <typename Y>
	auto for_pixel(Y callbck) -> void {
		for (u32 y = 0; y < size.y; y++) {
			for (u32 x = 0; x < size.x; x++) {
				u2_32 pos = this->pos + ty2<u32>(x, y);
				pixel_data &px = b.data.get()[pos.x + pos.y * b.vars.size_x];
				callbck(px);
#ifdef DEBUG_DUMP
				save_bmp("debug_log.bmp");
#endif
			}
		}
	}
	u2_32 pos;
	u2_32 size;
	Bitmap &b;
};
template <u16 Channels, typename Type>
class MultiChannelBitmap {
public:
	class data_deleter {
	public:
		void operator()(std::array<Type, Channels> *c) { delete[] c; }
	};
	struct vars {
		u2_32 size;

	} vars;
	MultiChannelBitmap(u2_32 size) {
		data = std::move(std::unique_ptr<std::array<Type, Channels>, data_deleter>(new std::array<Type, Channels>[size.get_product()]));
		vars.size = size;
	}
	MultiChannelBitmap(MultiChannelBitmap &&b) {
		vars = b.vars;
		data = std::move(data);
	};
	MultiChannelBitmap(MultiChannelBitmap &) = delete;
	~MultiChannelBitmap() = default;

	std::unique_ptr<std::array<Type, Channels>, data_deleter> data;
	constexpr inline auto get_channels_number() -> u16 { return Channels; };
	[[gnu::always_inline]] inline auto get_pixel(u2_32 pos) const {
		return data.get()[pos.x + pos.y * vars.size.x];
	}
	[[gnu::always_inline]] inline auto &get_pixel_ref(u2_32 pos) {
		return data.get()[pos.x + pos.y * vars.size.x];
	}
	[[gnu::always_inline]] inline auto set_pixel(u2_32 pos, std::array<Type, Channels> px) {
		data.get()[pos.x + pos.y * vars.size.x] = px;
	}
	auto save_binary(std::string path) -> void {
		std::ofstream file(path);
		file.write((u8 *)data.get(), vars.size.get_product() * sizeof(Type) * Channels);
	}
	auto save_bmp(std::string path) -> void {
		constexpr u8 sqr_size = ceil(sqrt(Channels));
		Bitmap b;
		b.crate(vars.size * ty2<u32>(sqr_size, sqr_size));
		for (u32 y = 0; y < vars.size.y; y++) {
			for (u32 x = 0; x < vars.size.x; x++) {
				for (u32 j = 0; j < sqr_size; j++) {
					for (u32 i = 0; i < sqr_size; i++) {
						pixel_data val;
						if (sqr_size * j + i < Channels) {
							val.rgba.r = ((u64)get_pixel({x, y})[j * sqr_size + i] * max_u8 / std::numeric_limits<Type>().max());
							val.rgba.a = 0xff;
						} else {
							val.data = 0x000000ff;
						}
						b.set_pixel({x * sqr_size + i, y * sqr_size + j}, val.data);
					}
				}
			}
		}
		b.save_bmp(path);
	}
};
template <u16 Channels, typename Type>
class MultiBitmapRef {
public:
	MultiBitmapRef(MultiChannelBitmap<Channels, Type> &map, u2_32 pos, u2_32 size)
			: map(map), pos(pos), size(size) {}
	MultiBitmapRef(MultiBitmapRef &&m) {
		size = m.size;
		pos = m.pos;
		map = m.map;
	};
	MultiBitmapRef(MultiBitmapRef &m) : map(m.map) {
		size = m.size;
		pos = m.pos;
	}
	auto inline get_pixel(u2_32 pos) { return map.get_pixel(this->pos + pos); }
	auto inline set_pixel(u2_32 pos, std::array<Type, Channels> px) {
		map.set_pixel(pos + this->pos, px);
	}
	inline auto &get_pixel_ref(u2_32 pos) {
		return map.get_pixel_ref(pos + this->pos);
	}
	auto for_pixel(auto callbck) -> void {
		for (u32 y = 0; y < size.y; y++) {
			for (u32 x = 0; x < size.x; x++) {
				u2_32 pos = this->pos + ty2<u32>(x, y);
				std::array<Type, Channels> px =
						map.data.get()[pos.x + pos.y * map.vars.size.x];
				callbck(px);
			}
		}
	}
	u2_32 size;
	u2_32 pos;

	MultiChannelBitmap<Channels, Type> &map;
};
