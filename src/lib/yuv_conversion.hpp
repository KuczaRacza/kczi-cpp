#pragma once
#include "utils.hpp"
#include <bitmap.hpp>
#define CLIP(X) ((X) > 255 ? 255 : (X) < 0 ? 0 : X)

// YUV -> RGB
#define C(Y) ((Y)-16)
#define D(U) ((U)-128)
#define E(V) ((V)-128)

#define YUV2R(Y, U, V) CLIP((298 * C(Y) + 409 * E(V) + 128) >> 8)
#define YUV2G(Y, U, V) CLIP((298 * C(Y) - 100 * D(U) - 208 * E(V) + 128) >> 8)
#define YUV2B(Y, U, V) CLIP((298 * C(Y) + 516 * D(U) + 128) >> 8)
// RGB -> YUV
#define RGB2Y(R, G, B) CLIP(((66 * (R) + 129 * (G) + 25 * (B) + 128) >> 8) + 16)
#define RGB2U(R, G, B) CLIP(((-38 * (R)-74 * (G) + 112 * (B) + 128) >> 8) + 128)
#define RGB2V(R, G, B) CLIP(((112 * (R)-94 * (G)-18 * (B) + 128) >> 8) + 128)
inline auto rgb_to_yuv(BitmapRef b) -> void {
  b.for_pixel([](pixel_data &px) {
    auto tmp = px;
    px.yuva.y = RGB2Y(tmp.rgba.r, tmp.rgba.g, tmp.rgba.b);
    px.yuva.u = RGB2U(tmp.rgba.r, tmp.rgba.g, tmp.rgba.b);
    px.yuva.v = RGB2V(tmp.rgba.r, tmp.rgba.g, tmp.rgba.b);
  });
}
inline auto yuv_to_rgb(BitmapRef b) -> void {
  b.for_pixel([](pixel_data &px) {
    auto tmp = px;
	px.rgba.r = YUV2R(tmp.yuva.y, tmp.yuva.u, tmp.yuva.v);
	px.rgba.g = YUV2G(tmp.yuva.y, tmp.yuva.u, tmp.yuva.v);
	px.rgba.b = YUV2B(tmp.yuva.y, tmp.yuva.u, tmp.yuva.v);

  });
}