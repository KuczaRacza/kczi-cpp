
#pragma once
#include <string>
#include <string_view>
#include <utility>

using namespace std::literals;
template <class F, class... Args,
					class = decltype(std::declval<F &&>()(std::declval<Args &&>()...))>
auto constexpr is_valid_impl(int)
		-> std::true_type;

template <class F, class... Args>
auto constexpr is_valid_impl(...)
		-> std::false_type;

template <class F>
constexpr auto is_valid(F &&) {
	return [](auto &&...args) {
		return decltype(is_valid_impl<F &&, decltype(args) &&...>(int{})){};
	};
}

//

namespace detail {
struct checker {
	auto static constexpr call(...) { return true; }
};
} // namespace detail

template <auto...>
struct auto_list {};

#define is_constexpr(...)   \
	is_valid([](auto checker) \
							 -> auto_list<decltype(checker)::call(__VA_ARGS__)> {})(detail::checker{})
