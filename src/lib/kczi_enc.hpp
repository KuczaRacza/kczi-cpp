#pragma once
#include "bitmap.hpp"
#include "cache.hpp"
#include "serialization.hpp"
#include "utils.hpp"
#include <array>
#include <region.hpp>
#include <unordered_map>
#include <unordered_set>
#include <vector>
enum class BLOCK_ALGO_INX : u8 {
	GRAD,
	FULL_RES
};
enum class BLOCK_ALGO_YUV : u8 {
	GRAD,
	SUBSAMPLED,
	PREDICT,
	DCT
};

class KcziEnc {
public:
	struct enc_args {
		u32 color_quant = 2;
		u32 dct_start = 16;
		u32 dct_end = 2048;
		u32 color_sensitivity = 30;
		u32 complecxity = 1;
		u32 block_size = 18;
		u8 alpha = 0;
		enum class enc_mode : u8 {
			automatic,
			indexed,
			yuv,
		} mode = enc_mode::yuv;
	};

	KcziEnc();
	~KcziEnc();
	Bitmap bitmap;
	// make this  atomic in future
	u32 dict_written = 0;
	auto encode(Bitmap &&bmap, enc_args args) -> std::vector<u8>;
	auto rect_tree_indexed(BitmapRef bmap) -> u8;
	auto recur_rect_tree_indexed(BitmapRef bmap, u8 depth) -> void;
	auto encode_indexed(Bitmap &b, enc_args args, DeserializedKczi &kczi) -> void;
	auto encode_yuv(Bitmap &b, enc_args args, DeserializedKczi &kczi) -> void;

	static auto region_enc(UnencRegion &reg, Dict &dict, enc_args e_args, u8 max_depth, KcziEnc &enc, u32 id) -> void;

	std::vector<UnencRegion> regions;
	std::vector<Dict> dicts;
	Cache cache;
};
