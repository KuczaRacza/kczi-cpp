#include "bitmap.hpp"
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_surface.h>
#include <filesystem>
#include <fstream>
#include <kczi_enc.hpp>
#include <utils.hpp>
#include <vector>
auto save_file(std::filesystem::path pth, std::vector<u8> file_content) {
  std::ofstream f;
  f.open(pth, std::ios::binary | std::ios::out);
  f.write((char *)file_content.data(), file_content.size());
  f.close();
}
int main(int argc, char **argv) {
  SDL_Surface *surf = IMG_Load(argv[1]);
  KcziEnc enc;
  KcziEnc::enc_args kczi_args;

  if (surf->format->format == SDL_PIXELFORMAT_RGBA32) {
    kczi_args.alpha = 1;
  }
  Bitmap bitmap;
  bitmap.load_img(surf);
  kczi_args.color_sensitivity = 20;
  kczi_args.color_quant = 0;
  kczi_args.block_size = 72;
  kczi_args.dct_start = 32;
  kczi_args.dct_end = 1024;
  auto out = enc.encode(std::move(bitmap), kczi_args);
  save_file(argv[2], out);
  return 0;
}