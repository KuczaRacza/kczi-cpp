#include "utils.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_surface.h>
#include <assert.h>
#include <blocks.hpp>
#include <constexpr_util.hpp>
#include <cstdio>
#include <kczi_dec.hpp>
#include <vector>


int main(int argc, char **argv) {

  std::vector<u8> file;
  read_file((std::string)argv[1], file);
  KcziDec k;
  u32 size_x;
  u32 size_y;
  u8 *data = k.decode(file, &size_x, &size_y);
  SDL_Surface *surf = SDL_CreateRGBSurfaceWithFormatFrom(
      data, size_x, size_y, 32, size_x * 4, SDL_PIXELFORMAT_RGBA32);
  SDL_SaveBMP(surf, argv[2]);
  return 0;
}
