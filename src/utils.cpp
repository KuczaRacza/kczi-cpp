#include "bitmap.hpp"
#include "filters.hpp"
#include <cstdio>
#include <kczi_dec.hpp>
#include <kczi_enc.hpp>
#include <serialization.hpp>
auto main(int argc, char **argv) -> int {
  std::vector<u8> file;
  read_file((std::string)argv[1], file);
  KcziDec k;
  DeserializedKczi kczi;
  kczi.deserialize(file);
  Bitmap image = k.decode_to_bitmap(file);
  for (u32 i = 2; i < (u32)argc; i++) {
    if ((std::string)argv[i] == "region-pos") {
      color_region_edges(kczi, image);
    } else if ((std::string)argv[i] == "blocks-grid") {
      color_blocks(kczi, image);
    } else if ((std::string)argv[i] == "dicts-id") {
      color_by_dict(kczi, image);
    } else {
      printf("unknow filter: %s \n", argv[i]);
    }
  }

  SDL_Surface *surf = image.crate_sdl();
  SDL_SaveBMP(surf, "f.bmp");
  return 0;
}