#include "cache.hpp"
#include "utils.hpp"
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_surface.h>
#include <bitmap.hpp>
#include <edges.hpp>
#include <string_view>
#include <unordered_map>
#include <vector>
#include <yuv_blocks.hpp>
int main(int argc, char **argv) {

	Bitmap b;
	SDL_Surface *surf = IMG_Load(argv[1]);
	b.load_img(surf);
	BitmapRef ref = {b, {0, 0}, {b.vars.size_x, b.vars.size_y}};
	Bitmap out;
	out.crate(ref.size);
	BitmapRef out_ref = {out, {0, 0}, {out.vars.size_x, out.vars.size_y}};
	std::vector<u8> enc_data;
	std::vector<u16> q_matrix(ref.size.get_product());
	enc_data.reserve(ref.size.get_product() * 2 * 3);
	Cache c;

	c.img_dct_quant = {15, 80};
	yuv_dct_enc(enc_data, ref, c);
	std::span<u8> enc_span(enc_data);
	yuv_dct_dec(enc_span, out_ref, c);
	SDL_Surface *sdl_out = out.crate_sdl();
	SDL_SaveBMP(sdl_out, argv[2]);
	SDL_FreeSurface(surf);
	SDL_FreeSurface(sdl_out);
	return 0;
}