cmake_minimum_required(VERSION 3.7)
project(kczipp)

include_directories("src/lib")
 


file(GLOB src "./src/lib/*.cpp")
file(GLOB filters "./src/filters.cpp")
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_FLAGS  "-Wpedantic -Wextra -fopenmp")
#set(CMAKE_CXX_FLAGS "-fsanitize=address,undefined")

include_directories("./src/lib")


add_executable(utils "./src/utils.cpp")
add_executable(dec "./src/dec.cpp")
add_executable(enc "./src/enc.cpp")
add_executable(dct "./src/dct_test.cpp")
add_executable(losstest "./test/losstest/losstest.cpp")
add_executable(img_type "./test/type_algo/main.cpp")


add_library( kczipp  SHARED ${src})


target_sources(dec PUBLIC ${src})
target_sources(dct PUBLIC ${src})
target_sources(enc PUBLIC ${src})
target_sources(utils PUBLIC ${src})
target_sources(utils PUBLIC ${filters})
target_sources(img_type PUBLIC ${src})



target_link_libraries(dec -lSDL2 -lSDL2_image -lzstd)
target_link_libraries(kczipp -lSDL2 -lSDL2_image -lzstd)
target_link_libraries(enc -lSDL2 -lSDL2_image -lzstd)
target_link_libraries(utils -lSDL2 -lSDL2_image -lzstd)
target_link_libraries(losstest -lSDL2 -lSDL2_image)
target_link_libraries(dct -lSDL2 -lSDL2_image -lzstd)
target_link_libraries(img_type -lSDL2 -lSDL2_image -lzstd)


