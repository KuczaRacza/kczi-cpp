#include "utils.hpp"
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_surface.h>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdint.h>

typedef uint32_t u32;
typedef uint16_t u16;
typedef int16_t i16;
typedef int32_t i32;
typedef uint8_t u8;
union pixel {
  u8 colors[4];
  u32 data;
};

int main(int argc, char **argv) {
  SDL_Surface *img = IMG_Load(argv[1]);
  SDL_Surface *ref = IMG_Load(argv[2]);
  double loss;
  u8 bpp;
  if (img->format->format == SDL_PIXELFORMAT_RGB24) {
    bpp = 3;
  } else {
    bpp = 4;
  }
  for (u32 y = 0; y < (u32)img->h; y++) {
    for (u32 x = 0; x < (u32)img->w; x++) {
      pixel main;
      pixel ref_px;
      memcpy(&main, (u8 *)img->pixels + img->pitch * y + x * bpp, bpp);
      memcpy(&ref_px, (u8 *)ref->pixels + ref->pitch * y + x * bpp, bpp);

      for (u32 c = 0; c < bpp; c++) {
        loss += pow(main.colors[c] - ref_px.colors[c], 2);
      }
    }
  }
  loss /= (img->w * img->h * bpp);
  printf("%f\n", loss);
  SDL_FreeSurface(img);
  SDL_FreeSurface(ref);
  return 0;
}