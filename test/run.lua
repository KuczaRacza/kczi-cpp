local lfs = require('lfs')
local os = require('os')

local bin_dir = "build/"
local stat_dir = "build/stat/"
local img_dir = "build/stat/"
local test_file = "build/0.jpg"
local options_enc = ""
local options_dec = ""
local all_res_file = "build/stat/stats.md"
local time = os.time(os.date("!*t"));
local kczi_name = img_dir .. "/" .. string.gsub(test_file, "/", "_") .. "_" ..
                      time

local function exec_enc()
    local command = "hyperfine \"./" .. bin_dir .. "enc " .. test_file .. " " ..
                        kczi_name .. ".kczi" .. options_enc ..
                        "\" --export-markdown " .. stat_dir .. time ..
                        string.gsub(test_file, "/", "_") .. "enc.md"
    local res = os.execute(command)
    if res == nil then
        print(
            "Something went wrong!\nヾ((●＞□＜)ﾉ*:..｡o○ＳＯЯЯЧ○o。..:*ヽ(＞□＜●))ﾉｼ\nCommand:" ..
                command .. "\n")
    else
        local out = io.open(all_res_file, "a")
        local f_in = io.open(stat_dir .. time ..
                                 string.gsub(test_file, "/", "_") .. "enc.md",
                             "r")
        io.input(f_in)
        local content = io.read("a") .. "\n"
        io.output(out)
        io.write(content)
        io.close(f_in)
        io.close(out)
        os.execute(
            "rm " .. stat_dir .. time .. string.gsub(test_file, "/", "_") ..
                "enc.md")
        os.execute("cp " .. bin_dir .. "enc" .. " " .. stat_dir .. "enc" .. time)
    end
end
local function exec_dec()
    local command = "hyperfine \"./" .. bin_dir .. "dec " .. kczi_name ..
                        ".kczi " .. kczi_name .. ".bmp " .. options_dec ..
                        "\" --export-markdown " .. stat_dir .. time ..
                        string.gsub(test_file, "/", "_") .. "dec.md"
    local res = os.execute(command)
    if res == nil then
        print(
            "Something went wrong!\nヾ((●＞□＜)ﾉ*:..｡o○ＳＯЯЯЧ○o。..:*ヽ(＞□＜●))ﾉｼ\nCommand:" ..
                command .. "\n")
    else
        local out = io.open(all_res_file, "a")
        local f_in = io.open(stat_dir .. time ..
                                 string.gsub(test_file, "/", "_") .. "dec.md",
                             "r")
        io.input(f_in)
        local content = io.read("a")
		content = content..'\n Size:'..lfs.attributes(kczi_name..".kczi","size")..'\n'
        io.output(out)
        io.write(content)
        io.close(f_in)
        io.close(out)
        os.execute(
            "rm " .. stat_dir .. time .. string.gsub(test_file, "/", "_") ..
                "dec.md")
        os.execute("cp " .. bin_dir .. "dec" .. " " .. stat_dir .. "dec" .. time)
    end
end
lfs.chdir("..")
exec_enc()
exec_dec()
