#include <SDL2/SDL_image.h>
#include <edges.hpp>
#include <yuv_conversion.hpp>
auto main(int argc, char **argv) -> int {
	Bitmap b;
	SDL_Surface *surf = IMG_Load(argv[1]);
	b.load_img(surf);
	BitmapRef ref = {b, {0, 0}, {b.vars.size_x, b.vars.size_y}};
	rgb_to_yuv(ref);
	u32 meadn_edges;
	auto edges_map = create_edges_map(ref, &meadn_edges);
	is_drawing(edges_map, meadn_edges, 32);
	return 0;
}